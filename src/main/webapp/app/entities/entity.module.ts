import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ProjetLicenceJobOfferModule } from './job-offer/job-offer.module';
import { ProjetLicenceCompanyModule } from './company/company.module';
import { ProjetLicenceCandidatModule } from './candidat/candidat.module';
import { ProjetLicenceSkillTestModule } from './skill-test/skill-test.module';
import { ProjetLicenceSkillTestResponseModule } from './skill-test-response/skill-test-response.module';
import { ProjetLicenceSkillQuestionModule } from './skill-question/skill-question.module';
import { ProjetLicenceSkillModule } from './skill/skill.module';
import { ProjetLicenceJobSkillModule } from './job-skill/job-skill.module';
import { ProjetLicenceJobResponseModule } from './job-response/job-response.module';
import { ProjetLicenceCategoryOfferModule } from './category-offer/category-offer.module';
import { ProjetLicenceProfilModule } from './profil/profil.module';
import { ProjetLicenceEtude_ProfilModule } from './etude-profil/etude-profil.module';
import { ProjetLicenceEtudeModule } from './etude/etude.module';
import { ProjetLicenceExperience_ProfilModule } from './experience-profil/experience-profil.module';
import { ProjetLicenceExperienceModule } from './experience/experience.module';
import { ProjetLicenceCandidate_skillModule } from './candidate-skill/candidate-skill.module';
import { ProjetLicenceCandidate_true_skillModule } from './candidate-true-skill/candidate-true-skill.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        ProjetLicenceJobOfferModule,
        ProjetLicenceCompanyModule,
        ProjetLicenceCandidatModule,
        ProjetLicenceSkillTestModule,
        ProjetLicenceSkillTestResponseModule,
        ProjetLicenceSkillQuestionModule,
        ProjetLicenceSkillModule,
        ProjetLicenceJobSkillModule,
        ProjetLicenceJobResponseModule,
        ProjetLicenceCategoryOfferModule,
        ProjetLicenceProfilModule,
        ProjetLicenceEtude_ProfilModule,
        ProjetLicenceEtudeModule,
        ProjetLicenceExperience_ProfilModule,
        ProjetLicenceExperienceModule,
        ProjetLicenceCandidate_skillModule,
        ProjetLicenceCandidate_true_skillModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetLicenceEntityModule {}
