import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Etude_Profil } from './etude-profil.model';
import { Etude_ProfilPopupService } from './etude-profil-popup.service';
import { Etude_ProfilService } from './etude-profil.service';

@Component({
    selector: 'jhi-etude-profil-delete-dialog',
    templateUrl: './etude-profil-delete-dialog.component.html'
})
export class Etude_ProfilDeleteDialogComponent {

    etude_Profil: Etude_Profil;

    constructor(
        private etude_ProfilService: Etude_ProfilService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.etude_ProfilService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'etude_ProfilListModification',
                content: 'Deleted an etude_Profil'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-etude-profil-delete-popup',
    template: ''
})
export class Etude_ProfilDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private etude_ProfilPopupService: Etude_ProfilPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.etude_ProfilPopupService
                .open(Etude_ProfilDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
