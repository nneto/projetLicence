import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Etude_Profil } from './etude-profil.model';
import { Etude_ProfilPopupService } from './etude-profil-popup.service';
import { Etude_ProfilService } from './etude-profil.service';
import { Profil, ProfilService } from '../profil';
import { Etude, EtudeService } from '../etude';

@Component({
    selector: 'jhi-etude-profil-dialog',
    templateUrl: './etude-profil-dialog.component.html'
})
export class Etude_ProfilDialogComponent implements OnInit {

    etude_Profil: Etude_Profil;
    isSaving: boolean;

    idprofils: Profil[];

    idetudes: Etude[];
    anneeEtudeDebutDp: any;
    anneEtudeFinDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private etude_ProfilService: Etude_ProfilService,
        private profilService: ProfilService,
        private etudeService: EtudeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.profilService
            .query({filter: 'etude_profil-is-null'})
            .subscribe((res: HttpResponse<Profil[]>) => {
                if (!this.etude_Profil.idProfil || !this.etude_Profil.idProfil.id) {
                    this.idprofils = res.body;
                } else {
                    this.profilService
                        .find(this.etude_Profil.idProfil.id)
                        .subscribe((subRes: HttpResponse<Profil>) => {
                            this.idprofils = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.etudeService
            .query({filter: 'etude_profil-is-null'})
            .subscribe((res: HttpResponse<Etude[]>) => {
                if (!this.etude_Profil.idEtude || !this.etude_Profil.idEtude.id) {
                    this.idetudes = res.body;
                } else {
                    this.etudeService
                        .find(this.etude_Profil.idEtude.id)
                        .subscribe((subRes: HttpResponse<Etude>) => {
                            this.idetudes = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.etude_Profil.id !== undefined) {
            this.subscribeToSaveResponse(
                this.etude_ProfilService.update(this.etude_Profil));
        } else {
            this.subscribeToSaveResponse(
                this.etude_ProfilService.create(this.etude_Profil));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Etude_Profil>>) {
        result.subscribe((res: HttpResponse<Etude_Profil>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Etude_Profil) {
        this.eventManager.broadcast({ name: 'etude_ProfilListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    trackEtudeById(index: number, item: Etude) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-etude-profil-popup',
    template: ''
})
export class Etude_ProfilPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private etude_ProfilPopupService: Etude_ProfilPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.etude_ProfilPopupService
                    .open(Etude_ProfilDialogComponent as Component, params['id']);
            } else {
                this.etude_ProfilPopupService
                    .open(Etude_ProfilDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
