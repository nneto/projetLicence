import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Etude_Profil } from './etude-profil.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Etude_Profil>;

@Injectable()
export class Etude_ProfilService {

    private resourceUrl =  SERVER_API_URL + 'api/etude-profils';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/etude-profils';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(etude_Profil: Etude_Profil): Observable<EntityResponseType> {
        const copy = this.convert(etude_Profil);
        return this.http.post<Etude_Profil>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(etude_Profil: Etude_Profil): Observable<EntityResponseType> {
        const copy = this.convert(etude_Profil);
        return this.http.put<Etude_Profil>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Etude_Profil>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Etude_Profil[]>> {
        const options = createRequestOption(req);
        return this.http.get<Etude_Profil[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Etude_Profil[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Etude_Profil[]>> {
        const options = createRequestOption(req);
        return this.http.get<Etude_Profil[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Etude_Profil[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Etude_Profil = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Etude_Profil[]>): HttpResponse<Etude_Profil[]> {
        const jsonResponse: Etude_Profil[] = res.body;
        const body: Etude_Profil[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Etude_Profil.
     */
    private convertItemFromServer(etude_Profil: Etude_Profil): Etude_Profil {
        const copy: Etude_Profil = Object.assign({}, etude_Profil);
        copy.anneeEtudeDebut = this.dateUtils
            .convertLocalDateFromServer(etude_Profil.anneeEtudeDebut);
        copy.anneEtudeFin = this.dateUtils
            .convertLocalDateFromServer(etude_Profil.anneEtudeFin);
        return copy;
    }

    /**
     * Convert a Etude_Profil to a JSON which can be sent to the server.
     */
    private convert(etude_Profil: Etude_Profil): Etude_Profil {
        const copy: Etude_Profil = Object.assign({}, etude_Profil);
        copy.anneeEtudeDebut = this.dateUtils
            .convertLocalDateToServer(etude_Profil.anneeEtudeDebut);
        copy.anneEtudeFin = this.dateUtils
            .convertLocalDateToServer(etude_Profil.anneEtudeFin);
        return copy;
    }
}
