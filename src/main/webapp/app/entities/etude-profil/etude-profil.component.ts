import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Etude_Profil } from './etude-profil.model';
import { Etude_ProfilService } from './etude-profil.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-etude-profil',
    templateUrl: './etude-profil.component.html'
})
export class Etude_ProfilComponent implements OnInit, OnDestroy {
etude_Profils: Etude_Profil[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private etude_ProfilService: Etude_ProfilService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.etude_ProfilService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<Etude_Profil[]>) => this.etude_Profils = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.etude_ProfilService.query().subscribe(
            (res: HttpResponse<Etude_Profil[]>) => {
                this.etude_Profils = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEtude_Profils();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Etude_Profil) {
        return item.id;
    }
    registerChangeInEtude_Profils() {
        this.eventSubscriber = this.eventManager.subscribe('etude_ProfilListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
