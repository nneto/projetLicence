import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetLicenceSharedModule } from '../../shared';
import {
    Etude_ProfilService,
    Etude_ProfilPopupService,
    Etude_ProfilComponent,
    Etude_ProfilDetailComponent,
    Etude_ProfilDialogComponent,
    Etude_ProfilPopupComponent,
    Etude_ProfilDeletePopupComponent,
    Etude_ProfilDeleteDialogComponent,
    etude_ProfilRoute,
    etude_ProfilPopupRoute,
} from './';

const ENTITY_STATES = [
    ...etude_ProfilRoute,
    ...etude_ProfilPopupRoute,
];

@NgModule({
    imports: [
        ProjetLicenceSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        Etude_ProfilComponent,
        Etude_ProfilDetailComponent,
        Etude_ProfilDialogComponent,
        Etude_ProfilDeleteDialogComponent,
        Etude_ProfilPopupComponent,
        Etude_ProfilDeletePopupComponent,
    ],
    entryComponents: [
        Etude_ProfilComponent,
        Etude_ProfilDialogComponent,
        Etude_ProfilPopupComponent,
        Etude_ProfilDeleteDialogComponent,
        Etude_ProfilDeletePopupComponent,
    ],
    providers: [
        Etude_ProfilService,
        Etude_ProfilPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetLicenceEtude_ProfilModule {}
