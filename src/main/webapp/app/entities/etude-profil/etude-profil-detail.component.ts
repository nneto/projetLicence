import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Etude_Profil } from './etude-profil.model';
import { Etude_ProfilService } from './etude-profil.service';

@Component({
    selector: 'jhi-etude-profil-detail',
    templateUrl: './etude-profil-detail.component.html'
})
export class Etude_ProfilDetailComponent implements OnInit, OnDestroy {

    etude_Profil: Etude_Profil;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private etude_ProfilService: Etude_ProfilService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEtude_Profils();
    }

    load(id) {
        this.etude_ProfilService.find(id)
            .subscribe((etude_ProfilResponse: HttpResponse<Etude_Profil>) => {
                this.etude_Profil = etude_ProfilResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEtude_Profils() {
        this.eventSubscriber = this.eventManager.subscribe(
            'etude_ProfilListModification',
            (response) => this.load(this.etude_Profil.id)
        );
    }
}
