import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { Etude_ProfilComponent } from './etude-profil.component';
import { Etude_ProfilDetailComponent } from './etude-profil-detail.component';
import { Etude_ProfilPopupComponent } from './etude-profil-dialog.component';
import { Etude_ProfilDeletePopupComponent } from './etude-profil-delete-dialog.component';

export const etude_ProfilRoute: Routes = [
    {
        path: 'etude-profil',
        component: Etude_ProfilComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Etude_Profils'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'etude-profil/:id',
        component: Etude_ProfilDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Etude_Profils'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const etude_ProfilPopupRoute: Routes = [
    {
        path: 'etude-profil-new',
        component: Etude_ProfilPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Etude_Profils'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'etude-profil/:id/edit',
        component: Etude_ProfilPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Etude_Profils'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'etude-profil/:id/delete',
        component: Etude_ProfilDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Etude_Profils'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
