import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Etude_Profil } from './etude-profil.model';
import { Etude_ProfilService } from './etude-profil.service';

@Injectable()
export class Etude_ProfilPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private etude_ProfilService: Etude_ProfilService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.etude_ProfilService.find(id)
                    .subscribe((etude_ProfilResponse: HttpResponse<Etude_Profil>) => {
                        const etude_Profil: Etude_Profil = etude_ProfilResponse.body;
                        if (etude_Profil.anneeEtudeDebut) {
                            etude_Profil.anneeEtudeDebut = {
                                year: etude_Profil.anneeEtudeDebut.getFullYear(),
                                month: etude_Profil.anneeEtudeDebut.getMonth() + 1,
                                day: etude_Profil.anneeEtudeDebut.getDate()
                            };
                        }
                        if (etude_Profil.anneEtudeFin) {
                            etude_Profil.anneEtudeFin = {
                                year: etude_Profil.anneEtudeFin.getFullYear(),
                                month: etude_Profil.anneEtudeFin.getMonth() + 1,
                                day: etude_Profil.anneEtudeFin.getDate()
                            };
                        }
                        this.ngbModalRef = this.etude_ProfilModalRef(component, etude_Profil);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.etude_ProfilModalRef(component, new Etude_Profil());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    etude_ProfilModalRef(component: Component, etude_Profil: Etude_Profil): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.etude_Profil = etude_Profil;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
