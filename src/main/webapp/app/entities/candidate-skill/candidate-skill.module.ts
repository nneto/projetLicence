import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetLicenceSharedModule } from '../../shared';
import {
    Candidate_skillService,
    Candidate_skillPopupService,
    Candidate_skillComponent,
    Candidate_skillDetailComponent,
    Candidate_skillDialogComponent,
    Candidate_skillPopupComponent,
    Candidate_skillDeletePopupComponent,
    Candidate_skillDeleteDialogComponent,
    candidate_skillRoute,
    candidate_skillPopupRoute,
} from './';

const ENTITY_STATES = [
    ...candidate_skillRoute,
    ...candidate_skillPopupRoute,
];

@NgModule({
    imports: [
        ProjetLicenceSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        Candidate_skillComponent,
        Candidate_skillDetailComponent,
        Candidate_skillDialogComponent,
        Candidate_skillDeleteDialogComponent,
        Candidate_skillPopupComponent,
        Candidate_skillDeletePopupComponent,
    ],
    entryComponents: [
        Candidate_skillComponent,
        Candidate_skillDialogComponent,
        Candidate_skillPopupComponent,
        Candidate_skillDeleteDialogComponent,
        Candidate_skillDeletePopupComponent,
    ],
    providers: [
        Candidate_skillService,
        Candidate_skillPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetLicenceCandidate_skillModule {}
