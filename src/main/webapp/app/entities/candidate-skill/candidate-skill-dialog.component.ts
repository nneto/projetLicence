import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Candidate_skill } from './candidate-skill.model';
import { Candidate_skillPopupService } from './candidate-skill-popup.service';
import { Candidate_skillService } from './candidate-skill.service';
import { Profil, ProfilService } from '../profil';
import { Skill, SkillService } from '../skill';

@Component({
    selector: 'jhi-candidate-skill-dialog',
    templateUrl: './candidate-skill-dialog.component.html'
})
export class Candidate_skillDialogComponent implements OnInit {

    candidate_skill: Candidate_skill;
    isSaving: boolean;

    idprofils: Profil[];

    idskills: Skill[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private candidate_skillService: Candidate_skillService,
        private profilService: ProfilService,
        private skillService: SkillService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.profilService
            .query({filter: 'candidate_skill-is-null'})
            .subscribe((res: HttpResponse<Profil[]>) => {
                if (!this.candidate_skill.idProfil || !this.candidate_skill.idProfil.id) {
                    this.idprofils = res.body;
                } else {
                    this.profilService
                        .find(this.candidate_skill.idProfil.id)
                        .subscribe((subRes: HttpResponse<Profil>) => {
                            this.idprofils = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.skillService
            .query({filter: 'candidate_skill-is-null'})
            .subscribe((res: HttpResponse<Skill[]>) => {
                if (!this.candidate_skill.idSkill || !this.candidate_skill.idSkill.id) {
                    this.idskills = res.body;
                } else {
                    this.skillService
                        .find(this.candidate_skill.idSkill.id)
                        .subscribe((subRes: HttpResponse<Skill>) => {
                            this.idskills = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.candidate_skill.id !== undefined) {
            this.subscribeToSaveResponse(
                this.candidate_skillService.update(this.candidate_skill));
        } else {
            this.subscribeToSaveResponse(
                this.candidate_skillService.create(this.candidate_skill));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Candidate_skill>>) {
        result.subscribe((res: HttpResponse<Candidate_skill>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Candidate_skill) {
        this.eventManager.broadcast({ name: 'candidate_skillListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    trackSkillById(index: number, item: Skill) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-candidate-skill-popup',
    template: ''
})
export class Candidate_skillPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private candidate_skillPopupService: Candidate_skillPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.candidate_skillPopupService
                    .open(Candidate_skillDialogComponent as Component, params['id']);
            } else {
                this.candidate_skillPopupService
                    .open(Candidate_skillDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
