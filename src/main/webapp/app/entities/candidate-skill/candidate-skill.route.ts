import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { Candidate_skillComponent } from './candidate-skill.component';
import { Candidate_skillDetailComponent } from './candidate-skill-detail.component';
import { Candidate_skillPopupComponent } from './candidate-skill-dialog.component';
import { Candidate_skillDeletePopupComponent } from './candidate-skill-delete-dialog.component';

export const candidate_skillRoute: Routes = [
    {
        path: 'candidate-skill',
        component: Candidate_skillComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_skills'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'candidate-skill/:id',
        component: Candidate_skillDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_skills'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const candidate_skillPopupRoute: Routes = [
    {
        path: 'candidate-skill-new',
        component: Candidate_skillPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_skills'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'candidate-skill/:id/edit',
        component: Candidate_skillPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_skills'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'candidate-skill/:id/delete',
        component: Candidate_skillDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_skills'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
