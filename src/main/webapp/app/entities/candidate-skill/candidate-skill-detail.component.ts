import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Candidate_skill } from './candidate-skill.model';
import { Candidate_skillService } from './candidate-skill.service';

@Component({
    selector: 'jhi-candidate-skill-detail',
    templateUrl: './candidate-skill-detail.component.html'
})
export class Candidate_skillDetailComponent implements OnInit, OnDestroy {

    candidate_skill: Candidate_skill;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private candidate_skillService: Candidate_skillService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCandidate_skills();
    }

    load(id) {
        this.candidate_skillService.find(id)
            .subscribe((candidate_skillResponse: HttpResponse<Candidate_skill>) => {
                this.candidate_skill = candidate_skillResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCandidate_skills() {
        this.eventSubscriber = this.eventManager.subscribe(
            'candidate_skillListModification',
            (response) => this.load(this.candidate_skill.id)
        );
    }
}
