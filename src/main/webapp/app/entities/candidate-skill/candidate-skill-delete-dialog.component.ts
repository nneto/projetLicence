import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Candidate_skill } from './candidate-skill.model';
import { Candidate_skillPopupService } from './candidate-skill-popup.service';
import { Candidate_skillService } from './candidate-skill.service';

@Component({
    selector: 'jhi-candidate-skill-delete-dialog',
    templateUrl: './candidate-skill-delete-dialog.component.html'
})
export class Candidate_skillDeleteDialogComponent {

    candidate_skill: Candidate_skill;

    constructor(
        private candidate_skillService: Candidate_skillService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.candidate_skillService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'candidate_skillListModification',
                content: 'Deleted an candidate_skill'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-candidate-skill-delete-popup',
    template: ''
})
export class Candidate_skillDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private candidate_skillPopupService: Candidate_skillPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.candidate_skillPopupService
                .open(Candidate_skillDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
