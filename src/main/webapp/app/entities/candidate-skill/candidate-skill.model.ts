import { BaseEntity } from './../../shared';

export class Candidate_skill implements BaseEntity {
    constructor(
        public id?: number,
        public level?: number,
        public comment?: string,
        public idProfil?: BaseEntity,
        public idSkill?: BaseEntity,
    ) {
    }
}
