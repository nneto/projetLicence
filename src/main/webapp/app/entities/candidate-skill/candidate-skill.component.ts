import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Candidate_skill } from './candidate-skill.model';
import { Candidate_skillService } from './candidate-skill.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-candidate-skill',
    templateUrl: './candidate-skill.component.html'
})
export class Candidate_skillComponent implements OnInit, OnDestroy {
candidate_skills: Candidate_skill[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private candidate_skillService: Candidate_skillService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.candidate_skillService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<Candidate_skill[]>) => this.candidate_skills = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.candidate_skillService.query().subscribe(
            (res: HttpResponse<Candidate_skill[]>) => {
                this.candidate_skills = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCandidate_skills();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Candidate_skill) {
        return item.id;
    }
    registerChangeInCandidate_skills() {
        this.eventSubscriber = this.eventManager.subscribe('candidate_skillListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
