import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Candidate_skill } from './candidate-skill.model';
import { Candidate_skillService } from './candidate-skill.service';

@Injectable()
export class Candidate_skillPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private candidate_skillService: Candidate_skillService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.candidate_skillService.find(id)
                    .subscribe((candidate_skillResponse: HttpResponse<Candidate_skill>) => {
                        const candidate_skill: Candidate_skill = candidate_skillResponse.body;
                        this.ngbModalRef = this.candidate_skillModalRef(component, candidate_skill);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.candidate_skillModalRef(component, new Candidate_skill());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    candidate_skillModalRef(component: Component, candidate_skill: Candidate_skill): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.candidate_skill = candidate_skill;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
