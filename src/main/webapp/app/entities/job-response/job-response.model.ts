import { BaseEntity } from './../../shared';

export class JobResponse implements BaseEntity {
    constructor(
        public id?: number,
        public dateResponse?: any,
        public comment?: string,
        public candidat?: BaseEntity,
        public jobOffer?: BaseEntity,
    ) {
    }
}
