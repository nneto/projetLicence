import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Experience_Profil } from './experience-profil.model';
import { Experience_ProfilService } from './experience-profil.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-experience-profil',
    templateUrl: './experience-profil.component.html'
})
export class Experience_ProfilComponent implements OnInit, OnDestroy {
experience_Profils: Experience_Profil[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private experience_ProfilService: Experience_ProfilService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.experience_ProfilService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<Experience_Profil[]>) => this.experience_Profils = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.experience_ProfilService.query().subscribe(
            (res: HttpResponse<Experience_Profil[]>) => {
                this.experience_Profils = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInExperience_Profils();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Experience_Profil) {
        return item.id;
    }
    registerChangeInExperience_Profils() {
        this.eventSubscriber = this.eventManager.subscribe('experience_ProfilListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
