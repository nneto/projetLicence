import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Experience_Profil } from './experience-profil.model';
import { Experience_ProfilPopupService } from './experience-profil-popup.service';
import { Experience_ProfilService } from './experience-profil.service';

@Component({
    selector: 'jhi-experience-profil-delete-dialog',
    templateUrl: './experience-profil-delete-dialog.component.html'
})
export class Experience_ProfilDeleteDialogComponent {

    experience_Profil: Experience_Profil;

    constructor(
        private experience_ProfilService: Experience_ProfilService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.experience_ProfilService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'experience_ProfilListModification',
                content: 'Deleted an experience_Profil'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-experience-profil-delete-popup',
    template: ''
})
export class Experience_ProfilDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private experience_ProfilPopupService: Experience_ProfilPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.experience_ProfilPopupService
                .open(Experience_ProfilDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
