import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Experience_Profil } from './experience-profil.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Experience_Profil>;

@Injectable()
export class Experience_ProfilService {

    private resourceUrl =  SERVER_API_URL + 'api/experience-profils';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/experience-profils';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(experience_Profil: Experience_Profil): Observable<EntityResponseType> {
        const copy = this.convert(experience_Profil);
        return this.http.post<Experience_Profil>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(experience_Profil: Experience_Profil): Observable<EntityResponseType> {
        const copy = this.convert(experience_Profil);
        return this.http.put<Experience_Profil>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Experience_Profil>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Experience_Profil[]>> {
        const options = createRequestOption(req);
        return this.http.get<Experience_Profil[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Experience_Profil[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Experience_Profil[]>> {
        const options = createRequestOption(req);
        return this.http.get<Experience_Profil[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Experience_Profil[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Experience_Profil = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Experience_Profil[]>): HttpResponse<Experience_Profil[]> {
        const jsonResponse: Experience_Profil[] = res.body;
        const body: Experience_Profil[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Experience_Profil.
     */
    private convertItemFromServer(experience_Profil: Experience_Profil): Experience_Profil {
        const copy: Experience_Profil = Object.assign({}, experience_Profil);
        copy.anneExperienceDebut = this.dateUtils
            .convertLocalDateFromServer(experience_Profil.anneExperienceDebut);
        copy.anneExperienceFin = this.dateUtils
            .convertLocalDateFromServer(experience_Profil.anneExperienceFin);
        return copy;
    }

    /**
     * Convert a Experience_Profil to a JSON which can be sent to the server.
     */
    private convert(experience_Profil: Experience_Profil): Experience_Profil {
        const copy: Experience_Profil = Object.assign({}, experience_Profil);
        copy.anneExperienceDebut = this.dateUtils
            .convertLocalDateToServer(experience_Profil.anneExperienceDebut);
        copy.anneExperienceFin = this.dateUtils
            .convertLocalDateToServer(experience_Profil.anneExperienceFin);
        return copy;
    }
}
