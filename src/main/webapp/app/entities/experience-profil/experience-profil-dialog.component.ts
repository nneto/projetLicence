import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Experience_Profil } from './experience-profil.model';
import { Experience_ProfilPopupService } from './experience-profil-popup.service';
import { Experience_ProfilService } from './experience-profil.service';
import { Profil, ProfilService } from '../profil';
import { Experience, ExperienceService } from '../experience';

@Component({
    selector: 'jhi-experience-profil-dialog',
    templateUrl: './experience-profil-dialog.component.html'
})
export class Experience_ProfilDialogComponent implements OnInit {

    experience_Profil: Experience_Profil;
    isSaving: boolean;

    idprofils: Profil[];

    idexperiences: Experience[];
    anneExperienceDebutDp: any;
    anneExperienceFinDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private experience_ProfilService: Experience_ProfilService,
        private profilService: ProfilService,
        private experienceService: ExperienceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.profilService
            .query({filter: 'experience_profil-is-null'})
            .subscribe((res: HttpResponse<Profil[]>) => {
                if (!this.experience_Profil.idProfil || !this.experience_Profil.idProfil.id) {
                    this.idprofils = res.body;
                } else {
                    this.profilService
                        .find(this.experience_Profil.idProfil.id)
                        .subscribe((subRes: HttpResponse<Profil>) => {
                            this.idprofils = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.experienceService
            .query({filter: 'experience_profil-is-null'})
            .subscribe((res: HttpResponse<Experience[]>) => {
                if (!this.experience_Profil.idExperience || !this.experience_Profil.idExperience.id) {
                    this.idexperiences = res.body;
                } else {
                    this.experienceService
                        .find(this.experience_Profil.idExperience.id)
                        .subscribe((subRes: HttpResponse<Experience>) => {
                            this.idexperiences = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.experience_Profil.id !== undefined) {
            this.subscribeToSaveResponse(
                this.experience_ProfilService.update(this.experience_Profil));
        } else {
            this.subscribeToSaveResponse(
                this.experience_ProfilService.create(this.experience_Profil));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Experience_Profil>>) {
        result.subscribe((res: HttpResponse<Experience_Profil>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Experience_Profil) {
        this.eventManager.broadcast({ name: 'experience_ProfilListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    trackExperienceById(index: number, item: Experience) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-experience-profil-popup',
    template: ''
})
export class Experience_ProfilPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private experience_ProfilPopupService: Experience_ProfilPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.experience_ProfilPopupService
                    .open(Experience_ProfilDialogComponent as Component, params['id']);
            } else {
                this.experience_ProfilPopupService
                    .open(Experience_ProfilDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
