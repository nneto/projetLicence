import { BaseEntity } from './../../shared';

export class Experience_Profil implements BaseEntity {
    constructor(
        public id?: number,
        public anneExperienceDebut?: any,
        public anneExperienceFin?: any,
        public comment?: string,
        public idProfil?: BaseEntity,
        public idExperience?: BaseEntity,
    ) {
    }
}
