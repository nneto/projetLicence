import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetLicenceSharedModule } from '../../shared';
import {
    Experience_ProfilService,
    Experience_ProfilPopupService,
    Experience_ProfilComponent,
    Experience_ProfilDetailComponent,
    Experience_ProfilDialogComponent,
    Experience_ProfilPopupComponent,
    Experience_ProfilDeletePopupComponent,
    Experience_ProfilDeleteDialogComponent,
    experience_ProfilRoute,
    experience_ProfilPopupRoute,
} from './';

const ENTITY_STATES = [
    ...experience_ProfilRoute,
    ...experience_ProfilPopupRoute,
];

@NgModule({
    imports: [
        ProjetLicenceSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        Experience_ProfilComponent,
        Experience_ProfilDetailComponent,
        Experience_ProfilDialogComponent,
        Experience_ProfilDeleteDialogComponent,
        Experience_ProfilPopupComponent,
        Experience_ProfilDeletePopupComponent,
    ],
    entryComponents: [
        Experience_ProfilComponent,
        Experience_ProfilDialogComponent,
        Experience_ProfilPopupComponent,
        Experience_ProfilDeleteDialogComponent,
        Experience_ProfilDeletePopupComponent,
    ],
    providers: [
        Experience_ProfilService,
        Experience_ProfilPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetLicenceExperience_ProfilModule {}
