import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { Experience_ProfilComponent } from './experience-profil.component';
import { Experience_ProfilDetailComponent } from './experience-profil-detail.component';
import { Experience_ProfilPopupComponent } from './experience-profil-dialog.component';
import { Experience_ProfilDeletePopupComponent } from './experience-profil-delete-dialog.component';

export const experience_ProfilRoute: Routes = [
    {
        path: 'experience-profil',
        component: Experience_ProfilComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Experience_Profils'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'experience-profil/:id',
        component: Experience_ProfilDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Experience_Profils'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const experience_ProfilPopupRoute: Routes = [
    {
        path: 'experience-profil-new',
        component: Experience_ProfilPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Experience_Profils'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'experience-profil/:id/edit',
        component: Experience_ProfilPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Experience_Profils'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'experience-profil/:id/delete',
        component: Experience_ProfilDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Experience_Profils'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
