import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Experience_Profil } from './experience-profil.model';
import { Experience_ProfilService } from './experience-profil.service';

@Injectable()
export class Experience_ProfilPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private experience_ProfilService: Experience_ProfilService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.experience_ProfilService.find(id)
                    .subscribe((experience_ProfilResponse: HttpResponse<Experience_Profil>) => {
                        const experience_Profil: Experience_Profil = experience_ProfilResponse.body;
                        if (experience_Profil.anneExperienceDebut) {
                            experience_Profil.anneExperienceDebut = {
                                year: experience_Profil.anneExperienceDebut.getFullYear(),
                                month: experience_Profil.anneExperienceDebut.getMonth() + 1,
                                day: experience_Profil.anneExperienceDebut.getDate()
                            };
                        }
                        if (experience_Profil.anneExperienceFin) {
                            experience_Profil.anneExperienceFin = {
                                year: experience_Profil.anneExperienceFin.getFullYear(),
                                month: experience_Profil.anneExperienceFin.getMonth() + 1,
                                day: experience_Profil.anneExperienceFin.getDate()
                            };
                        }
                        this.ngbModalRef = this.experience_ProfilModalRef(component, experience_Profil);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.experience_ProfilModalRef(component, new Experience_Profil());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    experience_ProfilModalRef(component: Component, experience_Profil: Experience_Profil): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.experience_Profil = experience_Profil;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
