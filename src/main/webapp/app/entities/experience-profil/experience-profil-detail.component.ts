import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Experience_Profil } from './experience-profil.model';
import { Experience_ProfilService } from './experience-profil.service';

@Component({
    selector: 'jhi-experience-profil-detail',
    templateUrl: './experience-profil-detail.component.html'
})
export class Experience_ProfilDetailComponent implements OnInit, OnDestroy {

    experience_Profil: Experience_Profil;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private experience_ProfilService: Experience_ProfilService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInExperience_Profils();
    }

    load(id) {
        this.experience_ProfilService.find(id)
            .subscribe((experience_ProfilResponse: HttpResponse<Experience_Profil>) => {
                this.experience_Profil = experience_ProfilResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInExperience_Profils() {
        this.eventSubscriber = this.eventManager.subscribe(
            'experience_ProfilListModification',
            (response) => this.load(this.experience_Profil.id)
        );
    }
}
