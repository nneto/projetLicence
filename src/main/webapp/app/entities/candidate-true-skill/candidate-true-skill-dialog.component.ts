import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Candidate_true_skill } from './candidate-true-skill.model';
import { Candidate_true_skillPopupService } from './candidate-true-skill-popup.service';
import { Candidate_true_skillService } from './candidate-true-skill.service';
import { Profil, ProfilService } from '../profil';
import { Skill, SkillService } from '../skill';

@Component({
    selector: 'jhi-candidate-true-skill-dialog',
    templateUrl: './candidate-true-skill-dialog.component.html'
})
export class Candidate_true_skillDialogComponent implements OnInit {

    candidate_true_skill: Candidate_true_skill;
    isSaving: boolean;

    idprofils: Profil[];

    idskills: Skill[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private candidate_true_skillService: Candidate_true_skillService,
        private profilService: ProfilService,
        private skillService: SkillService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.profilService
            .query({filter: 'candidate_true_skill-is-null'})
            .subscribe((res: HttpResponse<Profil[]>) => {
                if (!this.candidate_true_skill.idProfil || !this.candidate_true_skill.idProfil.id) {
                    this.idprofils = res.body;
                } else {
                    this.profilService
                        .find(this.candidate_true_skill.idProfil.id)
                        .subscribe((subRes: HttpResponse<Profil>) => {
                            this.idprofils = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.skillService
            .query({filter: 'candidate_true_skill-is-null'})
            .subscribe((res: HttpResponse<Skill[]>) => {
                if (!this.candidate_true_skill.idSkill || !this.candidate_true_skill.idSkill.id) {
                    this.idskills = res.body;
                } else {
                    this.skillService
                        .find(this.candidate_true_skill.idSkill.id)
                        .subscribe((subRes: HttpResponse<Skill>) => {
                            this.idskills = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.candidate_true_skill.id !== undefined) {
            this.subscribeToSaveResponse(
                this.candidate_true_skillService.update(this.candidate_true_skill));
        } else {
            this.subscribeToSaveResponse(
                this.candidate_true_skillService.create(this.candidate_true_skill));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Candidate_true_skill>>) {
        result.subscribe((res: HttpResponse<Candidate_true_skill>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Candidate_true_skill) {
        this.eventManager.broadcast({ name: 'candidate_true_skillListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    trackSkillById(index: number, item: Skill) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-candidate-true-skill-popup',
    template: ''
})
export class Candidate_true_skillPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private candidate_true_skillPopupService: Candidate_true_skillPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.candidate_true_skillPopupService
                    .open(Candidate_true_skillDialogComponent as Component, params['id']);
            } else {
                this.candidate_true_skillPopupService
                    .open(Candidate_true_skillDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
