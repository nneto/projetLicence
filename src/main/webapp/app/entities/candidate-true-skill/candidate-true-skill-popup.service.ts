import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Candidate_true_skill } from './candidate-true-skill.model';
import { Candidate_true_skillService } from './candidate-true-skill.service';

@Injectable()
export class Candidate_true_skillPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private candidate_true_skillService: Candidate_true_skillService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.candidate_true_skillService.find(id)
                    .subscribe((candidate_true_skillResponse: HttpResponse<Candidate_true_skill>) => {
                        const candidate_true_skill: Candidate_true_skill = candidate_true_skillResponse.body;
                        this.ngbModalRef = this.candidate_true_skillModalRef(component, candidate_true_skill);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.candidate_true_skillModalRef(component, new Candidate_true_skill());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    candidate_true_skillModalRef(component: Component, candidate_true_skill: Candidate_true_skill): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.candidate_true_skill = candidate_true_skill;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
