import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Candidate_true_skill } from './candidate-true-skill.model';
import { Candidate_true_skillService } from './candidate-true-skill.service';

@Component({
    selector: 'jhi-candidate-true-skill-detail',
    templateUrl: './candidate-true-skill-detail.component.html'
})
export class Candidate_true_skillDetailComponent implements OnInit, OnDestroy {

    candidate_true_skill: Candidate_true_skill;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private candidate_true_skillService: Candidate_true_skillService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCandidate_true_skills();
    }

    load(id) {
        this.candidate_true_skillService.find(id)
            .subscribe((candidate_true_skillResponse: HttpResponse<Candidate_true_skill>) => {
                this.candidate_true_skill = candidate_true_skillResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCandidate_true_skills() {
        this.eventSubscriber = this.eventManager.subscribe(
            'candidate_true_skillListModification',
            (response) => this.load(this.candidate_true_skill.id)
        );
    }
}
