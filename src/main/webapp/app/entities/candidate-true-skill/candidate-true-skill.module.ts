import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetLicenceSharedModule } from '../../shared';
import {
    Candidate_true_skillService,
    Candidate_true_skillPopupService,
    Candidate_true_skillComponent,
    Candidate_true_skillDetailComponent,
    Candidate_true_skillDialogComponent,
    Candidate_true_skillPopupComponent,
    Candidate_true_skillDeletePopupComponent,
    Candidate_true_skillDeleteDialogComponent,
    candidate_true_skillRoute,
    candidate_true_skillPopupRoute,
} from './';

const ENTITY_STATES = [
    ...candidate_true_skillRoute,
    ...candidate_true_skillPopupRoute,
];

@NgModule({
    imports: [
        ProjetLicenceSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        Candidate_true_skillComponent,
        Candidate_true_skillDetailComponent,
        Candidate_true_skillDialogComponent,
        Candidate_true_skillDeleteDialogComponent,
        Candidate_true_skillPopupComponent,
        Candidate_true_skillDeletePopupComponent,
    ],
    entryComponents: [
        Candidate_true_skillComponent,
        Candidate_true_skillDialogComponent,
        Candidate_true_skillPopupComponent,
        Candidate_true_skillDeleteDialogComponent,
        Candidate_true_skillDeletePopupComponent,
    ],
    providers: [
        Candidate_true_skillService,
        Candidate_true_skillPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetLicenceCandidate_true_skillModule {}
