import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Candidate_true_skill } from './candidate-true-skill.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Candidate_true_skill>;

@Injectable()
export class Candidate_true_skillService {

    private resourceUrl =  SERVER_API_URL + 'api/candidate-true-skills';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/candidate-true-skills';

    constructor(private http: HttpClient) { }

    create(candidate_true_skill: Candidate_true_skill): Observable<EntityResponseType> {
        const copy = this.convert(candidate_true_skill);
        return this.http.post<Candidate_true_skill>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(candidate_true_skill: Candidate_true_skill): Observable<EntityResponseType> {
        const copy = this.convert(candidate_true_skill);
        return this.http.put<Candidate_true_skill>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Candidate_true_skill>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Candidate_true_skill[]>> {
        const options = createRequestOption(req);
        return this.http.get<Candidate_true_skill[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Candidate_true_skill[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Candidate_true_skill[]>> {
        const options = createRequestOption(req);
        return this.http.get<Candidate_true_skill[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Candidate_true_skill[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Candidate_true_skill = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Candidate_true_skill[]>): HttpResponse<Candidate_true_skill[]> {
        const jsonResponse: Candidate_true_skill[] = res.body;
        const body: Candidate_true_skill[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Candidate_true_skill.
     */
    private convertItemFromServer(candidate_true_skill: Candidate_true_skill): Candidate_true_skill {
        const copy: Candidate_true_skill = Object.assign({}, candidate_true_skill);
        return copy;
    }

    /**
     * Convert a Candidate_true_skill to a JSON which can be sent to the server.
     */
    private convert(candidate_true_skill: Candidate_true_skill): Candidate_true_skill {
        const copy: Candidate_true_skill = Object.assign({}, candidate_true_skill);
        return copy;
    }
}
