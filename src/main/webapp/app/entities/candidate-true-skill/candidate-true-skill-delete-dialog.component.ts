import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Candidate_true_skill } from './candidate-true-skill.model';
import { Candidate_true_skillPopupService } from './candidate-true-skill-popup.service';
import { Candidate_true_skillService } from './candidate-true-skill.service';

@Component({
    selector: 'jhi-candidate-true-skill-delete-dialog',
    templateUrl: './candidate-true-skill-delete-dialog.component.html'
})
export class Candidate_true_skillDeleteDialogComponent {

    candidate_true_skill: Candidate_true_skill;

    constructor(
        private candidate_true_skillService: Candidate_true_skillService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.candidate_true_skillService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'candidate_true_skillListModification',
                content: 'Deleted an candidate_true_skill'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-candidate-true-skill-delete-popup',
    template: ''
})
export class Candidate_true_skillDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private candidate_true_skillPopupService: Candidate_true_skillPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.candidate_true_skillPopupService
                .open(Candidate_true_skillDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
