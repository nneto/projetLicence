import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { Candidate_true_skillComponent } from './candidate-true-skill.component';
import { Candidate_true_skillDetailComponent } from './candidate-true-skill-detail.component';
import { Candidate_true_skillPopupComponent } from './candidate-true-skill-dialog.component';
import { Candidate_true_skillDeletePopupComponent } from './candidate-true-skill-delete-dialog.component';

export const candidate_true_skillRoute: Routes = [
    {
        path: 'candidate-true-skill',
        component: Candidate_true_skillComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_true_skills'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'candidate-true-skill/:id',
        component: Candidate_true_skillDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_true_skills'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const candidate_true_skillPopupRoute: Routes = [
    {
        path: 'candidate-true-skill-new',
        component: Candidate_true_skillPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_true_skills'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'candidate-true-skill/:id/edit',
        component: Candidate_true_skillPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_true_skills'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'candidate-true-skill/:id/delete',
        component: Candidate_true_skillDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Candidate_true_skills'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
