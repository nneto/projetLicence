import { BaseEntity } from './../../shared';

export class Candidat implements BaseEntity {
    constructor(
        public id?: number,
        public profilId?: number,
    ) {
    }
}
