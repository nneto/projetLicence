import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetLicenceSharedModule } from '../../shared';
import {
    JobOfferService,
    JobOfferPopupService,
    JobOfferComponent,
    JobOfferDetailComponent,
    JobOfferDialogComponent,
    JobOfferPopupComponent,
    JobOfferDeletePopupComponent,
    JobOfferDeleteDialogComponent,
    jobOfferRoute,
    jobOfferPopupRoute,
    CompanyJobOfferComponent
} from './';

const ENTITY_STATES = [
    ...jobOfferRoute,
    ...jobOfferPopupRoute,
];

@NgModule({
    imports: [
        ProjetLicenceSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        JobOfferComponent,
        CompanyJobOfferComponent,
        JobOfferDetailComponent,
        JobOfferDialogComponent,
        JobOfferDeleteDialogComponent,
        JobOfferPopupComponent,
        JobOfferDeletePopupComponent,
    ],
    entryComponents: [
        JobOfferComponent,
        CompanyJobOfferComponent,
        JobOfferDialogComponent,
        JobOfferPopupComponent,
        JobOfferDeleteDialogComponent,
        JobOfferDeletePopupComponent,
    ],
    providers: [
        JobOfferService,
        JobOfferPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetLicenceJobOfferModule {}
