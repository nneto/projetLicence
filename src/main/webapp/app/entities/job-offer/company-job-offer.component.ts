import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JobOffer } from './job-offer.model';
import { JobOfferService } from './job-offer.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-company-job-offer',
    templateUrl: './company-job-offer.compenent.html'
})
export class CompanyJobOfferComponent implements OnInit, OnDestroy {
    jobOffers: JobOffer[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    idCompany:number;

    constructor(
        private jobOfferService: JobOfferService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.jobOfferService.search({
                query: this.currentSearch,
            }).subscribe(
                (res: HttpResponse<JobOffer[]>) => this.jobOffers = res.body,
                (res: HttpErrorResponse) => this.onError(res.message)
            );
            return;
        }
        this.jobOfferService.queryCompany(this.idCompany).subscribe(
            (res: HttpResponse<JobOffer[]>) => {
                this.jobOffers = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {

        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.activatedRoute.params.subscribe((params) => {
            this.idCompany=params['id'];

            this.loadAll();
        });
        this.registerChangeInJobOffers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: JobOffer) {
        return item.id;
    }
    registerChangeInJobOffers() {
        this.eventSubscriber = this.eventManager.subscribe('jobOfferListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
