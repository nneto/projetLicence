import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../app.constants';

import { JhiDateUtils } from 'ng-jhipster';
import { createRequestOption } from '../shared';
import {TypeOffre} from '../entities/job-offer/job-offer.model';
import {BaseEntity} from '../shared';

export class HomeData {
    constructor(
        public offer1?: string,
        public offer2?: string,
        public offer3?: string
    ) {
    }
}
@Injectable()
export class MyApiService {

    private resourceUrl =  SERVER_API_URL + 'api/job-offers';
    private resourceSearchUrl = SERVER_API_URL + 'v1/home';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    getHomeData(req?: any): Observable<HttpResponse<HomeData>> {
        const options = createRequestOption(req);
        return this.http.get<HomeData>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<HomeData>) => res);
    }

}
