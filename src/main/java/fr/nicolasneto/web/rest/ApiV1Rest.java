package fr.nicolasneto.web.rest;

import fr.nicolasneto.service.AuditEventService;
import fr.nicolasneto.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

/**
 * REST controller for getting the audit events.
 */
@RestController
@RequestMapping("/v1")
public class ApiV1Rest {
	public static class HomeData{
		public String Offer1="titi";
		public String Offer2="tata";
		public String Offer3="toto";	
	}
	@GetMapping("/home")
	public HomeData getHome(){
		return new HomeData();	
	}
	

}
