package fr.nicolasneto.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.nicolasneto.domain.Candidate_skill;

import fr.nicolasneto.repository.Candidate_skillRepository;
import fr.nicolasneto.repository.search.Candidate_skillSearchRepository;
import fr.nicolasneto.web.rest.errors.BadRequestAlertException;
import fr.nicolasneto.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Candidate_skill.
 */
@RestController
@RequestMapping("/api")
public class Candidate_skillResource {

    private final Logger log = LoggerFactory.getLogger(Candidate_skillResource.class);

    private static final String ENTITY_NAME = "candidate_skill";

    private final Candidate_skillRepository candidate_skillRepository;

    private final Candidate_skillSearchRepository candidate_skillSearchRepository;

    public Candidate_skillResource(Candidate_skillRepository candidate_skillRepository, Candidate_skillSearchRepository candidate_skillSearchRepository) {
        this.candidate_skillRepository = candidate_skillRepository;
        this.candidate_skillSearchRepository = candidate_skillSearchRepository;
    }

    /**
     * POST  /candidate-skills : Create a new candidate_skill.
     *
     * @param candidate_skill the candidate_skill to create
     * @return the ResponseEntity with status 201 (Created) and with body the new candidate_skill, or with status 400 (Bad Request) if the candidate_skill has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/candidate-skills")
    @Timed
    public ResponseEntity<Candidate_skill> createCandidate_skill(@RequestBody Candidate_skill candidate_skill) throws URISyntaxException {
        log.debug("REST request to save Candidate_skill : {}", candidate_skill);
        if (candidate_skill.getId() != null) {
            throw new BadRequestAlertException("A new candidate_skill cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Candidate_skill result = candidate_skillRepository.save(candidate_skill);
        candidate_skillSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/candidate-skills/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /candidate-skills : Updates an existing candidate_skill.
     *
     * @param candidate_skill the candidate_skill to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated candidate_skill,
     * or with status 400 (Bad Request) if the candidate_skill is not valid,
     * or with status 500 (Internal Server Error) if the candidate_skill couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/candidate-skills")
    @Timed
    public ResponseEntity<Candidate_skill> updateCandidate_skill(@RequestBody Candidate_skill candidate_skill) throws URISyntaxException {
        log.debug("REST request to update Candidate_skill : {}", candidate_skill);
        if (candidate_skill.getId() == null) {
            return createCandidate_skill(candidate_skill);
        }
        Candidate_skill result = candidate_skillRepository.save(candidate_skill);
        candidate_skillSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, candidate_skill.getId().toString()))
            .body(result);
    }

    /**
     * GET  /candidate-skills : get all the candidate_skills.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of candidate_skills in body
     */
    @GetMapping("/candidate-skills")
    @Timed
    public List<Candidate_skill> getAllCandidate_skills() {
        log.debug("REST request to get all Candidate_skills");
        return candidate_skillRepository.findAll();
        }

    /**
     * GET  /candidate-skills/:id : get the "id" candidate_skill.
     *
     * @param id the id of the candidate_skill to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the candidate_skill, or with status 404 (Not Found)
     */
    @GetMapping("/candidate-skills/{id}")
    @Timed
    public ResponseEntity<Candidate_skill> getCandidate_skill(@PathVariable Long id) {
        log.debug("REST request to get Candidate_skill : {}", id);
        Candidate_skill candidate_skill = candidate_skillRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(candidate_skill));
    }

    /**
     * DELETE  /candidate-skills/:id : delete the "id" candidate_skill.
     *
     * @param id the id of the candidate_skill to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/candidate-skills/{id}")
    @Timed
    public ResponseEntity<Void> deleteCandidate_skill(@PathVariable Long id) {
        log.debug("REST request to delete Candidate_skill : {}", id);
        candidate_skillRepository.delete(id);
        candidate_skillSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/candidate-skills?query=:query : search for the candidate_skill corresponding
     * to the query.
     *
     * @param query the query of the candidate_skill search
     * @return the result of the search
     */
    @GetMapping("/_search/candidate-skills")
    @Timed
    public List<Candidate_skill> searchCandidate_skills(@RequestParam String query) {
        log.debug("REST request to search Candidate_skills for query {}", query);
        return StreamSupport
            .stream(candidate_skillSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
