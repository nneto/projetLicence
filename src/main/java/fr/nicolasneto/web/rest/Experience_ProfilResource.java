package fr.nicolasneto.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.nicolasneto.domain.Experience_Profil;

import fr.nicolasneto.repository.Experience_ProfilRepository;
import fr.nicolasneto.repository.search.Experience_ProfilSearchRepository;
import fr.nicolasneto.web.rest.errors.BadRequestAlertException;
import fr.nicolasneto.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Experience_Profil.
 */
@RestController
@RequestMapping("/api")
public class Experience_ProfilResource {

    private final Logger log = LoggerFactory.getLogger(Experience_ProfilResource.class);

    private static final String ENTITY_NAME = "experience_Profil";

    private final Experience_ProfilRepository experience_ProfilRepository;

    private final Experience_ProfilSearchRepository experience_ProfilSearchRepository;

    public Experience_ProfilResource(Experience_ProfilRepository experience_ProfilRepository, Experience_ProfilSearchRepository experience_ProfilSearchRepository) {
        this.experience_ProfilRepository = experience_ProfilRepository;
        this.experience_ProfilSearchRepository = experience_ProfilSearchRepository;
    }

    /**
     * POST  /experience-profils : Create a new experience_Profil.
     *
     * @param experience_Profil the experience_Profil to create
     * @return the ResponseEntity with status 201 (Created) and with body the new experience_Profil, or with status 400 (Bad Request) if the experience_Profil has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/experience-profils")
    @Timed
    public ResponseEntity<Experience_Profil> createExperience_Profil(@Valid @RequestBody Experience_Profil experience_Profil) throws URISyntaxException {
        log.debug("REST request to save Experience_Profil : {}", experience_Profil);
        if (experience_Profil.getId() != null) {
            throw new BadRequestAlertException("A new experience_Profil cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Experience_Profil result = experience_ProfilRepository.save(experience_Profil);
        experience_ProfilSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/experience-profils/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /experience-profils : Updates an existing experience_Profil.
     *
     * @param experience_Profil the experience_Profil to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated experience_Profil,
     * or with status 400 (Bad Request) if the experience_Profil is not valid,
     * or with status 500 (Internal Server Error) if the experience_Profil couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/experience-profils")
    @Timed
    public ResponseEntity<Experience_Profil> updateExperience_Profil(@Valid @RequestBody Experience_Profil experience_Profil) throws URISyntaxException {
        log.debug("REST request to update Experience_Profil : {}", experience_Profil);
        if (experience_Profil.getId() == null) {
            return createExperience_Profil(experience_Profil);
        }
        Experience_Profil result = experience_ProfilRepository.save(experience_Profil);
        experience_ProfilSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, experience_Profil.getId().toString()))
            .body(result);
    }

    /**
     * GET  /experience-profils : get all the experience_Profils.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of experience_Profils in body
     */
    @GetMapping("/experience-profils")
    @Timed
    public List<Experience_Profil> getAllExperience_Profils() {
        log.debug("REST request to get all Experience_Profils");
        return experience_ProfilRepository.findAll();
        }

    /**
     * GET  /experience-profils/:id : get the "id" experience_Profil.
     *
     * @param id the id of the experience_Profil to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the experience_Profil, or with status 404 (Not Found)
     */
    @GetMapping("/experience-profils/{id}")
    @Timed
    public ResponseEntity<Experience_Profil> getExperience_Profil(@PathVariable Long id) {
        log.debug("REST request to get Experience_Profil : {}", id);
        Experience_Profil experience_Profil = experience_ProfilRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(experience_Profil));
    }

    /**
     * DELETE  /experience-profils/:id : delete the "id" experience_Profil.
     *
     * @param id the id of the experience_Profil to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/experience-profils/{id}")
    @Timed
    public ResponseEntity<Void> deleteExperience_Profil(@PathVariable Long id) {
        log.debug("REST request to delete Experience_Profil : {}", id);
        experience_ProfilRepository.delete(id);
        experience_ProfilSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/experience-profils?query=:query : search for the experience_Profil corresponding
     * to the query.
     *
     * @param query the query of the experience_Profil search
     * @return the result of the search
     */
    @GetMapping("/_search/experience-profils")
    @Timed
    public List<Experience_Profil> searchExperience_Profils(@RequestParam String query) {
        log.debug("REST request to search Experience_Profils for query {}", query);
        return StreamSupport
            .stream(experience_ProfilSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
