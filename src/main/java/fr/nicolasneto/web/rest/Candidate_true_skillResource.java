package fr.nicolasneto.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.nicolasneto.domain.Candidate_true_skill;

import fr.nicolasneto.repository.Candidate_true_skillRepository;
import fr.nicolasneto.repository.search.Candidate_true_skillSearchRepository;
import fr.nicolasneto.web.rest.errors.BadRequestAlertException;
import fr.nicolasneto.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Candidate_true_skill.
 */
@RestController
@RequestMapping("/api")
public class Candidate_true_skillResource {

    private final Logger log = LoggerFactory.getLogger(Candidate_true_skillResource.class);

    private static final String ENTITY_NAME = "candidate_true_skill";

    private final Candidate_true_skillRepository candidate_true_skillRepository;

    private final Candidate_true_skillSearchRepository candidate_true_skillSearchRepository;

    public Candidate_true_skillResource(Candidate_true_skillRepository candidate_true_skillRepository, Candidate_true_skillSearchRepository candidate_true_skillSearchRepository) {
        this.candidate_true_skillRepository = candidate_true_skillRepository;
        this.candidate_true_skillSearchRepository = candidate_true_skillSearchRepository;
    }

    /**
     * POST  /candidate-true-skills : Create a new candidate_true_skill.
     *
     * @param candidate_true_skill the candidate_true_skill to create
     * @return the ResponseEntity with status 201 (Created) and with body the new candidate_true_skill, or with status 400 (Bad Request) if the candidate_true_skill has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/candidate-true-skills")
    @Timed
    public ResponseEntity<Candidate_true_skill> createCandidate_true_skill(@Valid @RequestBody Candidate_true_skill candidate_true_skill) throws URISyntaxException {
        log.debug("REST request to save Candidate_true_skill : {}", candidate_true_skill);
        if (candidate_true_skill.getId() != null) {
            throw new BadRequestAlertException("A new candidate_true_skill cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Candidate_true_skill result = candidate_true_skillRepository.save(candidate_true_skill);
        candidate_true_skillSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/candidate-true-skills/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /candidate-true-skills : Updates an existing candidate_true_skill.
     *
     * @param candidate_true_skill the candidate_true_skill to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated candidate_true_skill,
     * or with status 400 (Bad Request) if the candidate_true_skill is not valid,
     * or with status 500 (Internal Server Error) if the candidate_true_skill couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/candidate-true-skills")
    @Timed
    public ResponseEntity<Candidate_true_skill> updateCandidate_true_skill(@Valid @RequestBody Candidate_true_skill candidate_true_skill) throws URISyntaxException {
        log.debug("REST request to update Candidate_true_skill : {}", candidate_true_skill);
        if (candidate_true_skill.getId() == null) {
            return createCandidate_true_skill(candidate_true_skill);
        }
        Candidate_true_skill result = candidate_true_skillRepository.save(candidate_true_skill);
        candidate_true_skillSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, candidate_true_skill.getId().toString()))
            .body(result);
    }

    /**
     * GET  /candidate-true-skills : get all the candidate_true_skills.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of candidate_true_skills in body
     */
    @GetMapping("/candidate-true-skills")
    @Timed
    public List<Candidate_true_skill> getAllCandidate_true_skills() {
        log.debug("REST request to get all Candidate_true_skills");
        return candidate_true_skillRepository.findAll();
        }

    /**
     * GET  /candidate-true-skills/:id : get the "id" candidate_true_skill.
     *
     * @param id the id of the candidate_true_skill to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the candidate_true_skill, or with status 404 (Not Found)
     */
    @GetMapping("/candidate-true-skills/{id}")
    @Timed
    public ResponseEntity<Candidate_true_skill> getCandidate_true_skill(@PathVariable Long id) {
        log.debug("REST request to get Candidate_true_skill : {}", id);
        Candidate_true_skill candidate_true_skill = candidate_true_skillRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(candidate_true_skill));
    }

    /**
     * DELETE  /candidate-true-skills/:id : delete the "id" candidate_true_skill.
     *
     * @param id the id of the candidate_true_skill to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/candidate-true-skills/{id}")
    @Timed
    public ResponseEntity<Void> deleteCandidate_true_skill(@PathVariable Long id) {
        log.debug("REST request to delete Candidate_true_skill : {}", id);
        candidate_true_skillRepository.delete(id);
        candidate_true_skillSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/candidate-true-skills?query=:query : search for the candidate_true_skill corresponding
     * to the query.
     *
     * @param query the query of the candidate_true_skill search
     * @return the result of the search
     */
    @GetMapping("/_search/candidate-true-skills")
    @Timed
    public List<Candidate_true_skill> searchCandidate_true_skills(@RequestParam String query) {
        log.debug("REST request to search Candidate_true_skills for query {}", query);
        return StreamSupport
            .stream(candidate_true_skillSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
