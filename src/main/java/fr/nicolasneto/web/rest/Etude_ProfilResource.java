package fr.nicolasneto.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.nicolasneto.domain.Etude_Profil;

import fr.nicolasneto.repository.Etude_ProfilRepository;
import fr.nicolasneto.repository.search.Etude_ProfilSearchRepository;
import fr.nicolasneto.web.rest.errors.BadRequestAlertException;
import fr.nicolasneto.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Etude_Profil.
 */
@RestController
@RequestMapping("/api")
public class Etude_ProfilResource {

    private final Logger log = LoggerFactory.getLogger(Etude_ProfilResource.class);

    private static final String ENTITY_NAME = "etude_Profil";

    private final Etude_ProfilRepository etude_ProfilRepository;

    private final Etude_ProfilSearchRepository etude_ProfilSearchRepository;

    public Etude_ProfilResource(Etude_ProfilRepository etude_ProfilRepository, Etude_ProfilSearchRepository etude_ProfilSearchRepository) {
        this.etude_ProfilRepository = etude_ProfilRepository;
        this.etude_ProfilSearchRepository = etude_ProfilSearchRepository;
    }

    /**
     * POST  /etude-profils : Create a new etude_Profil.
     *
     * @param etude_Profil the etude_Profil to create
     * @return the ResponseEntity with status 201 (Created) and with body the new etude_Profil, or with status 400 (Bad Request) if the etude_Profil has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/etude-profils")
    @Timed
    public ResponseEntity<Etude_Profil> createEtude_Profil(@RequestBody Etude_Profil etude_Profil) throws URISyntaxException {
        log.debug("REST request to save Etude_Profil : {}", etude_Profil);
        if (etude_Profil.getId() != null) {
            throw new BadRequestAlertException("A new etude_Profil cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Etude_Profil result = etude_ProfilRepository.save(etude_Profil);
        etude_ProfilSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/etude-profils/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /etude-profils : Updates an existing etude_Profil.
     *
     * @param etude_Profil the etude_Profil to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated etude_Profil,
     * or with status 400 (Bad Request) if the etude_Profil is not valid,
     * or with status 500 (Internal Server Error) if the etude_Profil couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/etude-profils")
    @Timed
    public ResponseEntity<Etude_Profil> updateEtude_Profil(@RequestBody Etude_Profil etude_Profil) throws URISyntaxException {
        log.debug("REST request to update Etude_Profil : {}", etude_Profil);
        if (etude_Profil.getId() == null) {
            return createEtude_Profil(etude_Profil);
        }
        Etude_Profil result = etude_ProfilRepository.save(etude_Profil);
        etude_ProfilSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, etude_Profil.getId().toString()))
            .body(result);
    }

    /**
     * GET  /etude-profils : get all the etude_Profils.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of etude_Profils in body
     */
    @GetMapping("/etude-profils")
    @Timed
    public List<Etude_Profil> getAllEtude_Profils() {
        log.debug("REST request to get all Etude_Profils");
        return etude_ProfilRepository.findAll();
        }

    /**
     * GET  /etude-profils/:id : get the "id" etude_Profil.
     *
     * @param id the id of the etude_Profil to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the etude_Profil, or with status 404 (Not Found)
     */
    @GetMapping("/etude-profils/{id}")
    @Timed
    public ResponseEntity<Etude_Profil> getEtude_Profil(@PathVariable Long id) {
        log.debug("REST request to get Etude_Profil : {}", id);
        Etude_Profil etude_Profil = etude_ProfilRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(etude_Profil));
    }

    /**
     * DELETE  /etude-profils/:id : delete the "id" etude_Profil.
     *
     * @param id the id of the etude_Profil to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/etude-profils/{id}")
    @Timed
    public ResponseEntity<Void> deleteEtude_Profil(@PathVariable Long id) {
        log.debug("REST request to delete Etude_Profil : {}", id);
        etude_ProfilRepository.delete(id);
        etude_ProfilSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/etude-profils?query=:query : search for the etude_Profil corresponding
     * to the query.
     *
     * @param query the query of the etude_Profil search
     * @return the result of the search
     */
    @GetMapping("/_search/etude-profils")
    @Timed
    public List<Etude_Profil> searchEtude_Profils(@RequestParam String query) {
        log.debug("REST request to search Etude_Profils for query {}", query);
        return StreamSupport
            .stream(etude_ProfilSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
