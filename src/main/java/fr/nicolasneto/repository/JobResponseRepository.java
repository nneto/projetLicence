package fr.nicolasneto.repository;

import fr.nicolasneto.domain.JobResponse;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JobResponse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobResponseRepository extends JpaRepository<JobResponse, Long> {

}
