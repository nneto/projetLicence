package fr.nicolasneto.repository.search;

import fr.nicolasneto.domain.Candidate_skill;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Candidate_skill entity.
 */
public interface Candidate_skillSearchRepository extends ElasticsearchRepository<Candidate_skill, Long> {
}
