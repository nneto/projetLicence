package fr.nicolasneto.repository.search;

import fr.nicolasneto.domain.Candidat;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Candidat entity.
 */
public interface CandidatSearchRepository extends ElasticsearchRepository<Candidat, Long> {
}
