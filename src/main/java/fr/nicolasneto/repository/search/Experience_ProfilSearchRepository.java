package fr.nicolasneto.repository.search;

import fr.nicolasneto.domain.Experience_Profil;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Experience_Profil entity.
 */
public interface Experience_ProfilSearchRepository extends ElasticsearchRepository<Experience_Profil, Long> {
}
