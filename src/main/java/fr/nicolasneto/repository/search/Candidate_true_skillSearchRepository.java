package fr.nicolasneto.repository.search;

import fr.nicolasneto.domain.Candidate_true_skill;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Candidate_true_skill entity.
 */
public interface Candidate_true_skillSearchRepository extends ElasticsearchRepository<Candidate_true_skill, Long> {
}
