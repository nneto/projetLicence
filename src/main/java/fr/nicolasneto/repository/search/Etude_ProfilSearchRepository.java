package fr.nicolasneto.repository.search;

import fr.nicolasneto.domain.Etude_Profil;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Etude_Profil entity.
 */
public interface Etude_ProfilSearchRepository extends ElasticsearchRepository<Etude_Profil, Long> {
}
