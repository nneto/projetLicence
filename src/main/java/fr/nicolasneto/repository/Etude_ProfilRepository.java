package fr.nicolasneto.repository;

import fr.nicolasneto.domain.Etude_Profil;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Etude_Profil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Etude_ProfilRepository extends JpaRepository<Etude_Profil, Long> {

}
