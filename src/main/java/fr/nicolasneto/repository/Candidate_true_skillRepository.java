package fr.nicolasneto.repository;

import fr.nicolasneto.domain.Candidate_true_skill;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Candidate_true_skill entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Candidate_true_skillRepository extends JpaRepository<Candidate_true_skill, Long> {

}
