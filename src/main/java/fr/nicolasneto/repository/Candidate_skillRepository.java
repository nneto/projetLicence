package fr.nicolasneto.repository;

import fr.nicolasneto.domain.Candidate_skill;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Candidate_skill entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Candidate_skillRepository extends JpaRepository<Candidate_skill, Long> {

}
