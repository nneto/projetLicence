package fr.nicolasneto.repository;

import fr.nicolasneto.domain.Experience_Profil;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Experience_Profil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Experience_ProfilRepository extends JpaRepository<Experience_Profil, Long> {

}
