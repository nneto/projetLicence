package fr.nicolasneto.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Etude_Profil.
 */
@Entity
@Table(name = "etude_profil")
@Document(indexName = "etude_profil")
public class Etude_Profil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "annee_etude_debut")
    private LocalDate anneeEtudeDebut;

    @Column(name = "anne_etude_fin")
    private LocalDate anneEtudeFin;

    @Column(name = "jhi_comment")
    private String comment;

    @OneToOne
    @JoinColumn(unique = true)
    private Profil idProfil;

    @OneToOne
    @JoinColumn(unique = true)
    private Etude idEtude;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getAnneeEtudeDebut() {
        return anneeEtudeDebut;
    }

    public Etude_Profil anneeEtudeDebut(LocalDate anneeEtudeDebut) {
        this.anneeEtudeDebut = anneeEtudeDebut;
        return this;
    }

    public void setAnneeEtudeDebut(LocalDate anneeEtudeDebut) {
        this.anneeEtudeDebut = anneeEtudeDebut;
    }

    public LocalDate getAnneEtudeFin() {
        return anneEtudeFin;
    }

    public Etude_Profil anneEtudeFin(LocalDate anneEtudeFin) {
        this.anneEtudeFin = anneEtudeFin;
        return this;
    }

    public void setAnneEtudeFin(LocalDate anneEtudeFin) {
        this.anneEtudeFin = anneEtudeFin;
    }

    public String getComment() {
        return comment;
    }

    public Etude_Profil comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Profil getIdProfil() {
        return idProfil;
    }

    public Etude_Profil idProfil(Profil profil) {
        this.idProfil = profil;
        return this;
    }

    public void setIdProfil(Profil profil) {
        this.idProfil = profil;
    }

    public Etude getIdEtude() {
        return idEtude;
    }

    public Etude_Profil idEtude(Etude etude) {
        this.idEtude = etude;
        return this;
    }

    public void setIdEtude(Etude etude) {
        this.idEtude = etude;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Etude_Profil etude_Profil = (Etude_Profil) o;
        if (etude_Profil.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), etude_Profil.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Etude_Profil{" +
            "id=" + getId() +
            ", anneeEtudeDebut='" + getAnneeEtudeDebut() + "'" +
            ", anneEtudeFin='" + getAnneEtudeFin() + "'" +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
