package fr.nicolasneto.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A JobResponse.
 */
@Entity
@Table(name = "job_response")
@Document(indexName = "jobresponse")
public class JobResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "date_response", nullable = false)
    private LocalDate dateResponse;

    @NotNull
    @Column(name = "jhi_comment", nullable = false)
    private String comment;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Candidat candidat;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private JobOffer jobOffer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateResponse() {
        return dateResponse;
    }

    public JobResponse dateResponse(LocalDate dateResponse) {
        this.dateResponse = dateResponse;
        return this;
    }

    public void setDateResponse(LocalDate dateResponse) {
        this.dateResponse = dateResponse;
    }

    public String getComment() {
        return comment;
    }

    public JobResponse comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Candidat getCandidat() {
        return candidat;
    }

    public JobResponse candidat(Candidat candidat) {
        this.candidat = candidat;
        return this;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }

    public JobOffer getJobOffer() {
        return jobOffer;
    }

    public JobResponse jobOffer(JobOffer jobOffer) {
        this.jobOffer = jobOffer;
        return this;
    }

    public void setJobOffer(JobOffer jobOffer) {
        this.jobOffer = jobOffer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JobResponse jobResponse = (JobResponse) o;
        if (jobResponse.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobResponse.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobResponse{" +
            "id=" + getId() +
            ", dateResponse='" + getDateResponse() + "'" +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
