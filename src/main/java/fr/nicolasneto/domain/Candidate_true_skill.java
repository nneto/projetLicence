package fr.nicolasneto.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Candidate_true_skill.
 */
@Entity
@Table(name = "candidate_true_skill")
@Document(indexName = "candidate_true_skill")
public class Candidate_true_skill implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "candidateskillid", nullable = false)
    private Long candidateskillid;

    @Column(name = "jhi_level")
    private Long level;

    @Column(name = "jhi_comment")
    private String comment;

    @OneToOne
    @JoinColumn(unique = true)
    private Profil idProfil;

    @OneToOne
    @JoinColumn(unique = true)
    private Skill idSkill;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCandidateskillid() {
        return candidateskillid;
    }

    public Candidate_true_skill candidateskillid(Long candidateskillid) {
        this.candidateskillid = candidateskillid;
        return this;
    }

    public void setCandidateskillid(Long candidateskillid) {
        this.candidateskillid = candidateskillid;
    }

    public Long getLevel() {
        return level;
    }

    public Candidate_true_skill level(Long level) {
        this.level = level;
        return this;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public String getComment() {
        return comment;
    }

    public Candidate_true_skill comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Profil getIdProfil() {
        return idProfil;
    }

    public Candidate_true_skill idProfil(Profil profil) {
        this.idProfil = profil;
        return this;
    }

    public void setIdProfil(Profil profil) {
        this.idProfil = profil;
    }

    public Skill getIdSkill() {
        return idSkill;
    }

    public Candidate_true_skill idSkill(Skill skill) {
        this.idSkill = skill;
        return this;
    }

    public void setIdSkill(Skill skill) {
        this.idSkill = skill;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Candidate_true_skill candidate_true_skill = (Candidate_true_skill) o;
        if (candidate_true_skill.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), candidate_true_skill.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Candidate_true_skill{" +
            "id=" + getId() +
            ", candidateskillid=" + getCandidateskillid() +
            ", level=" + getLevel() +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
