package fr.nicolasneto.web.rest;

import fr.nicolasneto.ProjetLicenceApp;

import fr.nicolasneto.domain.Experience_Profil;
import fr.nicolasneto.repository.Experience_ProfilRepository;
import fr.nicolasneto.repository.search.Experience_ProfilSearchRepository;
import fr.nicolasneto.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static fr.nicolasneto.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Experience_ProfilResource REST controller.
 *
 * @see Experience_ProfilResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjetLicenceApp.class)
public class Experience_ProfilResourceIntTest {

    private static final LocalDate DEFAULT_ANNE_EXPERIENCE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ANNE_EXPERIENCE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ANNE_EXPERIENCE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ANNE_EXPERIENCE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    @Autowired
    private Experience_ProfilRepository experience_ProfilRepository;

    @Autowired
    private Experience_ProfilSearchRepository experience_ProfilSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restExperience_ProfilMockMvc;

    private Experience_Profil experience_Profil;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Experience_ProfilResource experience_ProfilResource = new Experience_ProfilResource(experience_ProfilRepository, experience_ProfilSearchRepository);
        this.restExperience_ProfilMockMvc = MockMvcBuilders.standaloneSetup(experience_ProfilResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Experience_Profil createEntity(EntityManager em) {
        Experience_Profil experience_Profil = new Experience_Profil()
            .anneExperienceDebut(DEFAULT_ANNE_EXPERIENCE_DEBUT)
            .anneExperienceFin(DEFAULT_ANNE_EXPERIENCE_FIN)
            .comment(DEFAULT_COMMENT);
        return experience_Profil;
    }

    @Before
    public void initTest() {
        experience_ProfilSearchRepository.deleteAll();
        experience_Profil = createEntity(em);
    }

    @Test
    @Transactional
    public void createExperience_Profil() throws Exception {
        int databaseSizeBeforeCreate = experience_ProfilRepository.findAll().size();

        // Create the Experience_Profil
        restExperience_ProfilMockMvc.perform(post("/api/experience-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(experience_Profil)))
            .andExpect(status().isCreated());

        // Validate the Experience_Profil in the database
        List<Experience_Profil> experience_ProfilList = experience_ProfilRepository.findAll();
        assertThat(experience_ProfilList).hasSize(databaseSizeBeforeCreate + 1);
        Experience_Profil testExperience_Profil = experience_ProfilList.get(experience_ProfilList.size() - 1);
        assertThat(testExperience_Profil.getAnneExperienceDebut()).isEqualTo(DEFAULT_ANNE_EXPERIENCE_DEBUT);
        assertThat(testExperience_Profil.getAnneExperienceFin()).isEqualTo(DEFAULT_ANNE_EXPERIENCE_FIN);
        assertThat(testExperience_Profil.getComment()).isEqualTo(DEFAULT_COMMENT);

        // Validate the Experience_Profil in Elasticsearch
        Experience_Profil experience_ProfilEs = experience_ProfilSearchRepository.findOne(testExperience_Profil.getId());
        assertThat(experience_ProfilEs).isEqualToIgnoringGivenFields(testExperience_Profil);
    }

    @Test
    @Transactional
    public void createExperience_ProfilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = experience_ProfilRepository.findAll().size();

        // Create the Experience_Profil with an existing ID
        experience_Profil.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExperience_ProfilMockMvc.perform(post("/api/experience-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(experience_Profil)))
            .andExpect(status().isBadRequest());

        // Validate the Experience_Profil in the database
        List<Experience_Profil> experience_ProfilList = experience_ProfilRepository.findAll();
        assertThat(experience_ProfilList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAnneExperienceDebutIsRequired() throws Exception {
        int databaseSizeBeforeTest = experience_ProfilRepository.findAll().size();
        // set the field null
        experience_Profil.setAnneExperienceDebut(null);

        // Create the Experience_Profil, which fails.

        restExperience_ProfilMockMvc.perform(post("/api/experience-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(experience_Profil)))
            .andExpect(status().isBadRequest());

        List<Experience_Profil> experience_ProfilList = experience_ProfilRepository.findAll();
        assertThat(experience_ProfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAnneExperienceFinIsRequired() throws Exception {
        int databaseSizeBeforeTest = experience_ProfilRepository.findAll().size();
        // set the field null
        experience_Profil.setAnneExperienceFin(null);

        // Create the Experience_Profil, which fails.

        restExperience_ProfilMockMvc.perform(post("/api/experience-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(experience_Profil)))
            .andExpect(status().isBadRequest());

        List<Experience_Profil> experience_ProfilList = experience_ProfilRepository.findAll();
        assertThat(experience_ProfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllExperience_Profils() throws Exception {
        // Initialize the database
        experience_ProfilRepository.saveAndFlush(experience_Profil);

        // Get all the experience_ProfilList
        restExperience_ProfilMockMvc.perform(get("/api/experience-profils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(experience_Profil.getId().intValue())))
            .andExpect(jsonPath("$.[*].anneExperienceDebut").value(hasItem(DEFAULT_ANNE_EXPERIENCE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].anneExperienceFin").value(hasItem(DEFAULT_ANNE_EXPERIENCE_FIN.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void getExperience_Profil() throws Exception {
        // Initialize the database
        experience_ProfilRepository.saveAndFlush(experience_Profil);

        // Get the experience_Profil
        restExperience_ProfilMockMvc.perform(get("/api/experience-profils/{id}", experience_Profil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(experience_Profil.getId().intValue()))
            .andExpect(jsonPath("$.anneExperienceDebut").value(DEFAULT_ANNE_EXPERIENCE_DEBUT.toString()))
            .andExpect(jsonPath("$.anneExperienceFin").value(DEFAULT_ANNE_EXPERIENCE_FIN.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExperience_Profil() throws Exception {
        // Get the experience_Profil
        restExperience_ProfilMockMvc.perform(get("/api/experience-profils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExperience_Profil() throws Exception {
        // Initialize the database
        experience_ProfilRepository.saveAndFlush(experience_Profil);
        experience_ProfilSearchRepository.save(experience_Profil);
        int databaseSizeBeforeUpdate = experience_ProfilRepository.findAll().size();

        // Update the experience_Profil
        Experience_Profil updatedExperience_Profil = experience_ProfilRepository.findOne(experience_Profil.getId());
        // Disconnect from session so that the updates on updatedExperience_Profil are not directly saved in db
        em.detach(updatedExperience_Profil);
        updatedExperience_Profil
            .anneExperienceDebut(UPDATED_ANNE_EXPERIENCE_DEBUT)
            .anneExperienceFin(UPDATED_ANNE_EXPERIENCE_FIN)
            .comment(UPDATED_COMMENT);

        restExperience_ProfilMockMvc.perform(put("/api/experience-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExperience_Profil)))
            .andExpect(status().isOk());

        // Validate the Experience_Profil in the database
        List<Experience_Profil> experience_ProfilList = experience_ProfilRepository.findAll();
        assertThat(experience_ProfilList).hasSize(databaseSizeBeforeUpdate);
        Experience_Profil testExperience_Profil = experience_ProfilList.get(experience_ProfilList.size() - 1);
        assertThat(testExperience_Profil.getAnneExperienceDebut()).isEqualTo(UPDATED_ANNE_EXPERIENCE_DEBUT);
        assertThat(testExperience_Profil.getAnneExperienceFin()).isEqualTo(UPDATED_ANNE_EXPERIENCE_FIN);
        assertThat(testExperience_Profil.getComment()).isEqualTo(UPDATED_COMMENT);

        // Validate the Experience_Profil in Elasticsearch
        Experience_Profil experience_ProfilEs = experience_ProfilSearchRepository.findOne(testExperience_Profil.getId());
        assertThat(experience_ProfilEs).isEqualToIgnoringGivenFields(testExperience_Profil);
    }

    @Test
    @Transactional
    public void updateNonExistingExperience_Profil() throws Exception {
        int databaseSizeBeforeUpdate = experience_ProfilRepository.findAll().size();

        // Create the Experience_Profil

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restExperience_ProfilMockMvc.perform(put("/api/experience-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(experience_Profil)))
            .andExpect(status().isCreated());

        // Validate the Experience_Profil in the database
        List<Experience_Profil> experience_ProfilList = experience_ProfilRepository.findAll();
        assertThat(experience_ProfilList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteExperience_Profil() throws Exception {
        // Initialize the database
        experience_ProfilRepository.saveAndFlush(experience_Profil);
        experience_ProfilSearchRepository.save(experience_Profil);
        int databaseSizeBeforeDelete = experience_ProfilRepository.findAll().size();

        // Get the experience_Profil
        restExperience_ProfilMockMvc.perform(delete("/api/experience-profils/{id}", experience_Profil.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean experience_ProfilExistsInEs = experience_ProfilSearchRepository.exists(experience_Profil.getId());
        assertThat(experience_ProfilExistsInEs).isFalse();

        // Validate the database is empty
        List<Experience_Profil> experience_ProfilList = experience_ProfilRepository.findAll();
        assertThat(experience_ProfilList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchExperience_Profil() throws Exception {
        // Initialize the database
        experience_ProfilRepository.saveAndFlush(experience_Profil);
        experience_ProfilSearchRepository.save(experience_Profil);

        // Search the experience_Profil
        restExperience_ProfilMockMvc.perform(get("/api/_search/experience-profils?query=id:" + experience_Profil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(experience_Profil.getId().intValue())))
            .andExpect(jsonPath("$.[*].anneExperienceDebut").value(hasItem(DEFAULT_ANNE_EXPERIENCE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].anneExperienceFin").value(hasItem(DEFAULT_ANNE_EXPERIENCE_FIN.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Experience_Profil.class);
        Experience_Profil experience_Profil1 = new Experience_Profil();
        experience_Profil1.setId(1L);
        Experience_Profil experience_Profil2 = new Experience_Profil();
        experience_Profil2.setId(experience_Profil1.getId());
        assertThat(experience_Profil1).isEqualTo(experience_Profil2);
        experience_Profil2.setId(2L);
        assertThat(experience_Profil1).isNotEqualTo(experience_Profil2);
        experience_Profil1.setId(null);
        assertThat(experience_Profil1).isNotEqualTo(experience_Profil2);
    }
}
