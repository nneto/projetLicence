package fr.nicolasneto.web.rest;

import fr.nicolasneto.ProjetLicenceApp;

import fr.nicolasneto.domain.Candidate_true_skill;
import fr.nicolasneto.repository.Candidate_true_skillRepository;
import fr.nicolasneto.repository.search.Candidate_true_skillSearchRepository;
import fr.nicolasneto.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static fr.nicolasneto.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Candidate_true_skillResource REST controller.
 *
 * @see Candidate_true_skillResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjetLicenceApp.class)
public class Candidate_true_skillResourceIntTest {

    private static final Long DEFAULT_CANDIDATESKILLID = 1L;
    private static final Long UPDATED_CANDIDATESKILLID = 2L;

    private static final Long DEFAULT_LEVEL = 1L;
    private static final Long UPDATED_LEVEL = 2L;

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    @Autowired
    private Candidate_true_skillRepository candidate_true_skillRepository;

    @Autowired
    private Candidate_true_skillSearchRepository candidate_true_skillSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCandidate_true_skillMockMvc;

    private Candidate_true_skill candidate_true_skill;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Candidate_true_skillResource candidate_true_skillResource = new Candidate_true_skillResource(candidate_true_skillRepository, candidate_true_skillSearchRepository);
        this.restCandidate_true_skillMockMvc = MockMvcBuilders.standaloneSetup(candidate_true_skillResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Candidate_true_skill createEntity(EntityManager em) {
        Candidate_true_skill candidate_true_skill = new Candidate_true_skill()
            .candidateskillid(DEFAULT_CANDIDATESKILLID)
            .level(DEFAULT_LEVEL)
            .comment(DEFAULT_COMMENT);
        return candidate_true_skill;
    }

    @Before
    public void initTest() {
        candidate_true_skillSearchRepository.deleteAll();
        candidate_true_skill = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidate_true_skill() throws Exception {
        int databaseSizeBeforeCreate = candidate_true_skillRepository.findAll().size();

        // Create the Candidate_true_skill
        restCandidate_true_skillMockMvc.perform(post("/api/candidate-true-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidate_true_skill)))
            .andExpect(status().isCreated());

        // Validate the Candidate_true_skill in the database
        List<Candidate_true_skill> candidate_true_skillList = candidate_true_skillRepository.findAll();
        assertThat(candidate_true_skillList).hasSize(databaseSizeBeforeCreate + 1);
        Candidate_true_skill testCandidate_true_skill = candidate_true_skillList.get(candidate_true_skillList.size() - 1);
        assertThat(testCandidate_true_skill.getCandidateskillid()).isEqualTo(DEFAULT_CANDIDATESKILLID);
        assertThat(testCandidate_true_skill.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testCandidate_true_skill.getComment()).isEqualTo(DEFAULT_COMMENT);

        // Validate the Candidate_true_skill in Elasticsearch
        Candidate_true_skill candidate_true_skillEs = candidate_true_skillSearchRepository.findOne(testCandidate_true_skill.getId());
        assertThat(candidate_true_skillEs).isEqualToIgnoringGivenFields(testCandidate_true_skill);
    }

    @Test
    @Transactional
    public void createCandidate_true_skillWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidate_true_skillRepository.findAll().size();

        // Create the Candidate_true_skill with an existing ID
        candidate_true_skill.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidate_true_skillMockMvc.perform(post("/api/candidate-true-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidate_true_skill)))
            .andExpect(status().isBadRequest());

        // Validate the Candidate_true_skill in the database
        List<Candidate_true_skill> candidate_true_skillList = candidate_true_skillRepository.findAll();
        assertThat(candidate_true_skillList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCandidateskillidIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidate_true_skillRepository.findAll().size();
        // set the field null
        candidate_true_skill.setCandidateskillid(null);

        // Create the Candidate_true_skill, which fails.

        restCandidate_true_skillMockMvc.perform(post("/api/candidate-true-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidate_true_skill)))
            .andExpect(status().isBadRequest());

        List<Candidate_true_skill> candidate_true_skillList = candidate_true_skillRepository.findAll();
        assertThat(candidate_true_skillList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCandidate_true_skills() throws Exception {
        // Initialize the database
        candidate_true_skillRepository.saveAndFlush(candidate_true_skill);

        // Get all the candidate_true_skillList
        restCandidate_true_skillMockMvc.perform(get("/api/candidate-true-skills?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidate_true_skill.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidateskillid").value(hasItem(DEFAULT_CANDIDATESKILLID.intValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.intValue())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void getCandidate_true_skill() throws Exception {
        // Initialize the database
        candidate_true_skillRepository.saveAndFlush(candidate_true_skill);

        // Get the candidate_true_skill
        restCandidate_true_skillMockMvc.perform(get("/api/candidate-true-skills/{id}", candidate_true_skill.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(candidate_true_skill.getId().intValue()))
            .andExpect(jsonPath("$.candidateskillid").value(DEFAULT_CANDIDATESKILLID.intValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL.intValue()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidate_true_skill() throws Exception {
        // Get the candidate_true_skill
        restCandidate_true_skillMockMvc.perform(get("/api/candidate-true-skills/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidate_true_skill() throws Exception {
        // Initialize the database
        candidate_true_skillRepository.saveAndFlush(candidate_true_skill);
        candidate_true_skillSearchRepository.save(candidate_true_skill);
        int databaseSizeBeforeUpdate = candidate_true_skillRepository.findAll().size();

        // Update the candidate_true_skill
        Candidate_true_skill updatedCandidate_true_skill = candidate_true_skillRepository.findOne(candidate_true_skill.getId());
        // Disconnect from session so that the updates on updatedCandidate_true_skill are not directly saved in db
        em.detach(updatedCandidate_true_skill);
        updatedCandidate_true_skill
            .candidateskillid(UPDATED_CANDIDATESKILLID)
            .level(UPDATED_LEVEL)
            .comment(UPDATED_COMMENT);

        restCandidate_true_skillMockMvc.perform(put("/api/candidate-true-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCandidate_true_skill)))
            .andExpect(status().isOk());

        // Validate the Candidate_true_skill in the database
        List<Candidate_true_skill> candidate_true_skillList = candidate_true_skillRepository.findAll();
        assertThat(candidate_true_skillList).hasSize(databaseSizeBeforeUpdate);
        Candidate_true_skill testCandidate_true_skill = candidate_true_skillList.get(candidate_true_skillList.size() - 1);
        assertThat(testCandidate_true_skill.getCandidateskillid()).isEqualTo(UPDATED_CANDIDATESKILLID);
        assertThat(testCandidate_true_skill.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testCandidate_true_skill.getComment()).isEqualTo(UPDATED_COMMENT);

        // Validate the Candidate_true_skill in Elasticsearch
        Candidate_true_skill candidate_true_skillEs = candidate_true_skillSearchRepository.findOne(testCandidate_true_skill.getId());
        assertThat(candidate_true_skillEs).isEqualToIgnoringGivenFields(testCandidate_true_skill);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidate_true_skill() throws Exception {
        int databaseSizeBeforeUpdate = candidate_true_skillRepository.findAll().size();

        // Create the Candidate_true_skill

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCandidate_true_skillMockMvc.perform(put("/api/candidate-true-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidate_true_skill)))
            .andExpect(status().isCreated());

        // Validate the Candidate_true_skill in the database
        List<Candidate_true_skill> candidate_true_skillList = candidate_true_skillRepository.findAll();
        assertThat(candidate_true_skillList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCandidate_true_skill() throws Exception {
        // Initialize the database
        candidate_true_skillRepository.saveAndFlush(candidate_true_skill);
        candidate_true_skillSearchRepository.save(candidate_true_skill);
        int databaseSizeBeforeDelete = candidate_true_skillRepository.findAll().size();

        // Get the candidate_true_skill
        restCandidate_true_skillMockMvc.perform(delete("/api/candidate-true-skills/{id}", candidate_true_skill.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean candidate_true_skillExistsInEs = candidate_true_skillSearchRepository.exists(candidate_true_skill.getId());
        assertThat(candidate_true_skillExistsInEs).isFalse();

        // Validate the database is empty
        List<Candidate_true_skill> candidate_true_skillList = candidate_true_skillRepository.findAll();
        assertThat(candidate_true_skillList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCandidate_true_skill() throws Exception {
        // Initialize the database
        candidate_true_skillRepository.saveAndFlush(candidate_true_skill);
        candidate_true_skillSearchRepository.save(candidate_true_skill);

        // Search the candidate_true_skill
        restCandidate_true_skillMockMvc.perform(get("/api/_search/candidate-true-skills?query=id:" + candidate_true_skill.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidate_true_skill.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidateskillid").value(hasItem(DEFAULT_CANDIDATESKILLID.intValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.intValue())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Candidate_true_skill.class);
        Candidate_true_skill candidate_true_skill1 = new Candidate_true_skill();
        candidate_true_skill1.setId(1L);
        Candidate_true_skill candidate_true_skill2 = new Candidate_true_skill();
        candidate_true_skill2.setId(candidate_true_skill1.getId());
        assertThat(candidate_true_skill1).isEqualTo(candidate_true_skill2);
        candidate_true_skill2.setId(2L);
        assertThat(candidate_true_skill1).isNotEqualTo(candidate_true_skill2);
        candidate_true_skill1.setId(null);
        assertThat(candidate_true_skill1).isNotEqualTo(candidate_true_skill2);
    }
}
