package fr.nicolasneto.web.rest;

import fr.nicolasneto.ProjetLicenceApp;

import fr.nicolasneto.domain.Etude_Profil;
import fr.nicolasneto.repository.Etude_ProfilRepository;
import fr.nicolasneto.repository.search.Etude_ProfilSearchRepository;
import fr.nicolasneto.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static fr.nicolasneto.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Etude_ProfilResource REST controller.
 *
 * @see Etude_ProfilResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjetLicenceApp.class)
public class Etude_ProfilResourceIntTest {

    private static final LocalDate DEFAULT_ANNEE_ETUDE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ANNEE_ETUDE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ANNE_ETUDE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ANNE_ETUDE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    @Autowired
    private Etude_ProfilRepository etude_ProfilRepository;

    @Autowired
    private Etude_ProfilSearchRepository etude_ProfilSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEtude_ProfilMockMvc;

    private Etude_Profil etude_Profil;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Etude_ProfilResource etude_ProfilResource = new Etude_ProfilResource(etude_ProfilRepository, etude_ProfilSearchRepository);
        this.restEtude_ProfilMockMvc = MockMvcBuilders.standaloneSetup(etude_ProfilResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etude_Profil createEntity(EntityManager em) {
        Etude_Profil etude_Profil = new Etude_Profil()
            .anneeEtudeDebut(DEFAULT_ANNEE_ETUDE_DEBUT)
            .anneEtudeFin(DEFAULT_ANNE_ETUDE_FIN)
            .comment(DEFAULT_COMMENT);
        return etude_Profil;
    }

    @Before
    public void initTest() {
        etude_ProfilSearchRepository.deleteAll();
        etude_Profil = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtude_Profil() throws Exception {
        int databaseSizeBeforeCreate = etude_ProfilRepository.findAll().size();

        // Create the Etude_Profil
        restEtude_ProfilMockMvc.perform(post("/api/etude-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etude_Profil)))
            .andExpect(status().isCreated());

        // Validate the Etude_Profil in the database
        List<Etude_Profil> etude_ProfilList = etude_ProfilRepository.findAll();
        assertThat(etude_ProfilList).hasSize(databaseSizeBeforeCreate + 1);
        Etude_Profil testEtude_Profil = etude_ProfilList.get(etude_ProfilList.size() - 1);
        assertThat(testEtude_Profil.getAnneeEtudeDebut()).isEqualTo(DEFAULT_ANNEE_ETUDE_DEBUT);
        assertThat(testEtude_Profil.getAnneEtudeFin()).isEqualTo(DEFAULT_ANNE_ETUDE_FIN);
        assertThat(testEtude_Profil.getComment()).isEqualTo(DEFAULT_COMMENT);

        // Validate the Etude_Profil in Elasticsearch
        Etude_Profil etude_ProfilEs = etude_ProfilSearchRepository.findOne(testEtude_Profil.getId());
        assertThat(etude_ProfilEs).isEqualToIgnoringGivenFields(testEtude_Profil);
    }

    @Test
    @Transactional
    public void createEtude_ProfilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etude_ProfilRepository.findAll().size();

        // Create the Etude_Profil with an existing ID
        etude_Profil.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtude_ProfilMockMvc.perform(post("/api/etude-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etude_Profil)))
            .andExpect(status().isBadRequest());

        // Validate the Etude_Profil in the database
        List<Etude_Profil> etude_ProfilList = etude_ProfilRepository.findAll();
        assertThat(etude_ProfilList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEtude_Profils() throws Exception {
        // Initialize the database
        etude_ProfilRepository.saveAndFlush(etude_Profil);

        // Get all the etude_ProfilList
        restEtude_ProfilMockMvc.perform(get("/api/etude-profils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etude_Profil.getId().intValue())))
            .andExpect(jsonPath("$.[*].anneeEtudeDebut").value(hasItem(DEFAULT_ANNEE_ETUDE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].anneEtudeFin").value(hasItem(DEFAULT_ANNE_ETUDE_FIN.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void getEtude_Profil() throws Exception {
        // Initialize the database
        etude_ProfilRepository.saveAndFlush(etude_Profil);

        // Get the etude_Profil
        restEtude_ProfilMockMvc.perform(get("/api/etude-profils/{id}", etude_Profil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(etude_Profil.getId().intValue()))
            .andExpect(jsonPath("$.anneeEtudeDebut").value(DEFAULT_ANNEE_ETUDE_DEBUT.toString()))
            .andExpect(jsonPath("$.anneEtudeFin").value(DEFAULT_ANNE_ETUDE_FIN.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEtude_Profil() throws Exception {
        // Get the etude_Profil
        restEtude_ProfilMockMvc.perform(get("/api/etude-profils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtude_Profil() throws Exception {
        // Initialize the database
        etude_ProfilRepository.saveAndFlush(etude_Profil);
        etude_ProfilSearchRepository.save(etude_Profil);
        int databaseSizeBeforeUpdate = etude_ProfilRepository.findAll().size();

        // Update the etude_Profil
        Etude_Profil updatedEtude_Profil = etude_ProfilRepository.findOne(etude_Profil.getId());
        // Disconnect from session so that the updates on updatedEtude_Profil are not directly saved in db
        em.detach(updatedEtude_Profil);
        updatedEtude_Profil
            .anneeEtudeDebut(UPDATED_ANNEE_ETUDE_DEBUT)
            .anneEtudeFin(UPDATED_ANNE_ETUDE_FIN)
            .comment(UPDATED_COMMENT);

        restEtude_ProfilMockMvc.perform(put("/api/etude-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEtude_Profil)))
            .andExpect(status().isOk());

        // Validate the Etude_Profil in the database
        List<Etude_Profil> etude_ProfilList = etude_ProfilRepository.findAll();
        assertThat(etude_ProfilList).hasSize(databaseSizeBeforeUpdate);
        Etude_Profil testEtude_Profil = etude_ProfilList.get(etude_ProfilList.size() - 1);
        assertThat(testEtude_Profil.getAnneeEtudeDebut()).isEqualTo(UPDATED_ANNEE_ETUDE_DEBUT);
        assertThat(testEtude_Profil.getAnneEtudeFin()).isEqualTo(UPDATED_ANNE_ETUDE_FIN);
        assertThat(testEtude_Profil.getComment()).isEqualTo(UPDATED_COMMENT);

        // Validate the Etude_Profil in Elasticsearch
        Etude_Profil etude_ProfilEs = etude_ProfilSearchRepository.findOne(testEtude_Profil.getId());
        assertThat(etude_ProfilEs).isEqualToIgnoringGivenFields(testEtude_Profil);
    }

    @Test
    @Transactional
    public void updateNonExistingEtude_Profil() throws Exception {
        int databaseSizeBeforeUpdate = etude_ProfilRepository.findAll().size();

        // Create the Etude_Profil

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEtude_ProfilMockMvc.perform(put("/api/etude-profils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(etude_Profil)))
            .andExpect(status().isCreated());

        // Validate the Etude_Profil in the database
        List<Etude_Profil> etude_ProfilList = etude_ProfilRepository.findAll();
        assertThat(etude_ProfilList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEtude_Profil() throws Exception {
        // Initialize the database
        etude_ProfilRepository.saveAndFlush(etude_Profil);
        etude_ProfilSearchRepository.save(etude_Profil);
        int databaseSizeBeforeDelete = etude_ProfilRepository.findAll().size();

        // Get the etude_Profil
        restEtude_ProfilMockMvc.perform(delete("/api/etude-profils/{id}", etude_Profil.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean etude_ProfilExistsInEs = etude_ProfilSearchRepository.exists(etude_Profil.getId());
        assertThat(etude_ProfilExistsInEs).isFalse();

        // Validate the database is empty
        List<Etude_Profil> etude_ProfilList = etude_ProfilRepository.findAll();
        assertThat(etude_ProfilList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchEtude_Profil() throws Exception {
        // Initialize the database
        etude_ProfilRepository.saveAndFlush(etude_Profil);
        etude_ProfilSearchRepository.save(etude_Profil);

        // Search the etude_Profil
        restEtude_ProfilMockMvc.perform(get("/api/_search/etude-profils?query=id:" + etude_Profil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etude_Profil.getId().intValue())))
            .andExpect(jsonPath("$.[*].anneeEtudeDebut").value(hasItem(DEFAULT_ANNEE_ETUDE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].anneEtudeFin").value(hasItem(DEFAULT_ANNE_ETUDE_FIN.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Etude_Profil.class);
        Etude_Profil etude_Profil1 = new Etude_Profil();
        etude_Profil1.setId(1L);
        Etude_Profil etude_Profil2 = new Etude_Profil();
        etude_Profil2.setId(etude_Profil1.getId());
        assertThat(etude_Profil1).isEqualTo(etude_Profil2);
        etude_Profil2.setId(2L);
        assertThat(etude_Profil1).isNotEqualTo(etude_Profil2);
        etude_Profil1.setId(null);
        assertThat(etude_Profil1).isNotEqualTo(etude_Profil2);
    }
}
