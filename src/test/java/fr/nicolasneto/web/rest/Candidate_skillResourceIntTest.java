package fr.nicolasneto.web.rest;

import fr.nicolasneto.ProjetLicenceApp;

import fr.nicolasneto.domain.Candidate_skill;
import fr.nicolasneto.repository.Candidate_skillRepository;
import fr.nicolasneto.repository.search.Candidate_skillSearchRepository;
import fr.nicolasneto.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static fr.nicolasneto.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Candidate_skillResource REST controller.
 *
 * @see Candidate_skillResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjetLicenceApp.class)
public class Candidate_skillResourceIntTest {

    private static final Long DEFAULT_LEVEL = 1L;
    private static final Long UPDATED_LEVEL = 2L;

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    @Autowired
    private Candidate_skillRepository candidate_skillRepository;

    @Autowired
    private Candidate_skillSearchRepository candidate_skillSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCandidate_skillMockMvc;

    private Candidate_skill candidate_skill;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Candidate_skillResource candidate_skillResource = new Candidate_skillResource(candidate_skillRepository, candidate_skillSearchRepository);
        this.restCandidate_skillMockMvc = MockMvcBuilders.standaloneSetup(candidate_skillResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Candidate_skill createEntity(EntityManager em) {
        Candidate_skill candidate_skill = new Candidate_skill()
            .level(DEFAULT_LEVEL)
            .comment(DEFAULT_COMMENT);
        return candidate_skill;
    }

    @Before
    public void initTest() {
        candidate_skillSearchRepository.deleteAll();
        candidate_skill = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidate_skill() throws Exception {
        int databaseSizeBeforeCreate = candidate_skillRepository.findAll().size();

        // Create the Candidate_skill
        restCandidate_skillMockMvc.perform(post("/api/candidate-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidate_skill)))
            .andExpect(status().isCreated());

        // Validate the Candidate_skill in the database
        List<Candidate_skill> candidate_skillList = candidate_skillRepository.findAll();
        assertThat(candidate_skillList).hasSize(databaseSizeBeforeCreate + 1);
        Candidate_skill testCandidate_skill = candidate_skillList.get(candidate_skillList.size() - 1);
        assertThat(testCandidate_skill.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testCandidate_skill.getComment()).isEqualTo(DEFAULT_COMMENT);

        // Validate the Candidate_skill in Elasticsearch
        Candidate_skill candidate_skillEs = candidate_skillSearchRepository.findOne(testCandidate_skill.getId());
        assertThat(candidate_skillEs).isEqualToIgnoringGivenFields(testCandidate_skill);
    }

    @Test
    @Transactional
    public void createCandidate_skillWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidate_skillRepository.findAll().size();

        // Create the Candidate_skill with an existing ID
        candidate_skill.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidate_skillMockMvc.perform(post("/api/candidate-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidate_skill)))
            .andExpect(status().isBadRequest());

        // Validate the Candidate_skill in the database
        List<Candidate_skill> candidate_skillList = candidate_skillRepository.findAll();
        assertThat(candidate_skillList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCandidate_skills() throws Exception {
        // Initialize the database
        candidate_skillRepository.saveAndFlush(candidate_skill);

        // Get all the candidate_skillList
        restCandidate_skillMockMvc.perform(get("/api/candidate-skills?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidate_skill.getId().intValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.intValue())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void getCandidate_skill() throws Exception {
        // Initialize the database
        candidate_skillRepository.saveAndFlush(candidate_skill);

        // Get the candidate_skill
        restCandidate_skillMockMvc.perform(get("/api/candidate-skills/{id}", candidate_skill.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(candidate_skill.getId().intValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL.intValue()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidate_skill() throws Exception {
        // Get the candidate_skill
        restCandidate_skillMockMvc.perform(get("/api/candidate-skills/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidate_skill() throws Exception {
        // Initialize the database
        candidate_skillRepository.saveAndFlush(candidate_skill);
        candidate_skillSearchRepository.save(candidate_skill);
        int databaseSizeBeforeUpdate = candidate_skillRepository.findAll().size();

        // Update the candidate_skill
        Candidate_skill updatedCandidate_skill = candidate_skillRepository.findOne(candidate_skill.getId());
        // Disconnect from session so that the updates on updatedCandidate_skill are not directly saved in db
        em.detach(updatedCandidate_skill);
        updatedCandidate_skill
            .level(UPDATED_LEVEL)
            .comment(UPDATED_COMMENT);

        restCandidate_skillMockMvc.perform(put("/api/candidate-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCandidate_skill)))
            .andExpect(status().isOk());

        // Validate the Candidate_skill in the database
        List<Candidate_skill> candidate_skillList = candidate_skillRepository.findAll();
        assertThat(candidate_skillList).hasSize(databaseSizeBeforeUpdate);
        Candidate_skill testCandidate_skill = candidate_skillList.get(candidate_skillList.size() - 1);
        assertThat(testCandidate_skill.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testCandidate_skill.getComment()).isEqualTo(UPDATED_COMMENT);

        // Validate the Candidate_skill in Elasticsearch
        Candidate_skill candidate_skillEs = candidate_skillSearchRepository.findOne(testCandidate_skill.getId());
        assertThat(candidate_skillEs).isEqualToIgnoringGivenFields(testCandidate_skill);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidate_skill() throws Exception {
        int databaseSizeBeforeUpdate = candidate_skillRepository.findAll().size();

        // Create the Candidate_skill

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCandidate_skillMockMvc.perform(put("/api/candidate-skills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidate_skill)))
            .andExpect(status().isCreated());

        // Validate the Candidate_skill in the database
        List<Candidate_skill> candidate_skillList = candidate_skillRepository.findAll();
        assertThat(candidate_skillList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCandidate_skill() throws Exception {
        // Initialize the database
        candidate_skillRepository.saveAndFlush(candidate_skill);
        candidate_skillSearchRepository.save(candidate_skill);
        int databaseSizeBeforeDelete = candidate_skillRepository.findAll().size();

        // Get the candidate_skill
        restCandidate_skillMockMvc.perform(delete("/api/candidate-skills/{id}", candidate_skill.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean candidate_skillExistsInEs = candidate_skillSearchRepository.exists(candidate_skill.getId());
        assertThat(candidate_skillExistsInEs).isFalse();

        // Validate the database is empty
        List<Candidate_skill> candidate_skillList = candidate_skillRepository.findAll();
        assertThat(candidate_skillList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCandidate_skill() throws Exception {
        // Initialize the database
        candidate_skillRepository.saveAndFlush(candidate_skill);
        candidate_skillSearchRepository.save(candidate_skill);

        // Search the candidate_skill
        restCandidate_skillMockMvc.perform(get("/api/_search/candidate-skills?query=id:" + candidate_skill.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidate_skill.getId().intValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.intValue())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Candidate_skill.class);
        Candidate_skill candidate_skill1 = new Candidate_skill();
        candidate_skill1.setId(1L);
        Candidate_skill candidate_skill2 = new Candidate_skill();
        candidate_skill2.setId(candidate_skill1.getId());
        assertThat(candidate_skill1).isEqualTo(candidate_skill2);
        candidate_skill2.setId(2L);
        assertThat(candidate_skill1).isNotEqualTo(candidate_skill2);
        candidate_skill1.setId(null);
        assertThat(candidate_skill1).isNotEqualTo(candidate_skill2);
    }
}
