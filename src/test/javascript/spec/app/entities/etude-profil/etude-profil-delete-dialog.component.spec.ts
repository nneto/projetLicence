/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Etude_ProfilDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil-delete-dialog.component';
import { Etude_ProfilService } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.service';

describe('Component Tests', () => {

    describe('Etude_Profil Management Delete Component', () => {
        let comp: Etude_ProfilDeleteDialogComponent;
        let fixture: ComponentFixture<Etude_ProfilDeleteDialogComponent>;
        let service: Etude_ProfilService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Etude_ProfilDeleteDialogComponent],
                providers: [
                    Etude_ProfilService
                ]
            })
            .overrideTemplate(Etude_ProfilDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Etude_ProfilDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Etude_ProfilService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
