/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Etude_ProfilDialogComponent } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil-dialog.component';
import { Etude_ProfilService } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.service';
import { Etude_Profil } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.model';
import { ProfilService } from '../../../../../../main/webapp/app/entities/profil';
import { EtudeService } from '../../../../../../main/webapp/app/entities/etude';

describe('Component Tests', () => {

    describe('Etude_Profil Management Dialog Component', () => {
        let comp: Etude_ProfilDialogComponent;
        let fixture: ComponentFixture<Etude_ProfilDialogComponent>;
        let service: Etude_ProfilService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Etude_ProfilDialogComponent],
                providers: [
                    ProfilService,
                    EtudeService,
                    Etude_ProfilService
                ]
            })
            .overrideTemplate(Etude_ProfilDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Etude_ProfilDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Etude_ProfilService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Etude_Profil(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.etude_Profil = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'etude_ProfilListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Etude_Profil();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.etude_Profil = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'etude_ProfilListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
