/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Etude_ProfilDetailComponent } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil-detail.component';
import { Etude_ProfilService } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.service';
import { Etude_Profil } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.model';

describe('Component Tests', () => {

    describe('Etude_Profil Management Detail Component', () => {
        let comp: Etude_ProfilDetailComponent;
        let fixture: ComponentFixture<Etude_ProfilDetailComponent>;
        let service: Etude_ProfilService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Etude_ProfilDetailComponent],
                providers: [
                    Etude_ProfilService
                ]
            })
            .overrideTemplate(Etude_ProfilDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Etude_ProfilDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Etude_ProfilService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Etude_Profil(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.etude_Profil).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
