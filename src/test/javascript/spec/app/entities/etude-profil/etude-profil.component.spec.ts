/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Etude_ProfilComponent } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.component';
import { Etude_ProfilService } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.service';
import { Etude_Profil } from '../../../../../../main/webapp/app/entities/etude-profil/etude-profil.model';

describe('Component Tests', () => {

    describe('Etude_Profil Management Component', () => {
        let comp: Etude_ProfilComponent;
        let fixture: ComponentFixture<Etude_ProfilComponent>;
        let service: Etude_ProfilService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Etude_ProfilComponent],
                providers: [
                    Etude_ProfilService
                ]
            })
            .overrideTemplate(Etude_ProfilComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Etude_ProfilComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Etude_ProfilService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Etude_Profil(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.etude_Profils[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
