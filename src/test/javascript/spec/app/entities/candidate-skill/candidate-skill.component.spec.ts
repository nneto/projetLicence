/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Candidate_skillComponent } from '../../../../../../main/webapp/app/entities/candidate-skill/candidate-skill.component';
import { Candidate_skillService } from '../../../../../../main/webapp/app/entities/candidate-skill/candidate-skill.service';
import { Candidate_skill } from '../../../../../../main/webapp/app/entities/candidate-skill/candidate-skill.model';

describe('Component Tests', () => {

    describe('Candidate_skill Management Component', () => {
        let comp: Candidate_skillComponent;
        let fixture: ComponentFixture<Candidate_skillComponent>;
        let service: Candidate_skillService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Candidate_skillComponent],
                providers: [
                    Candidate_skillService
                ]
            })
            .overrideTemplate(Candidate_skillComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Candidate_skillComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Candidate_skillService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Candidate_skill(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.candidate_skills[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
