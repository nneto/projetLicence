/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Candidate_skillDetailComponent } from '../../../../../../main/webapp/app/entities/candidate-skill/candidate-skill-detail.component';
import { Candidate_skillService } from '../../../../../../main/webapp/app/entities/candidate-skill/candidate-skill.service';
import { Candidate_skill } from '../../../../../../main/webapp/app/entities/candidate-skill/candidate-skill.model';

describe('Component Tests', () => {

    describe('Candidate_skill Management Detail Component', () => {
        let comp: Candidate_skillDetailComponent;
        let fixture: ComponentFixture<Candidate_skillDetailComponent>;
        let service: Candidate_skillService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Candidate_skillDetailComponent],
                providers: [
                    Candidate_skillService
                ]
            })
            .overrideTemplate(Candidate_skillDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Candidate_skillDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Candidate_skillService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Candidate_skill(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.candidate_skill).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
