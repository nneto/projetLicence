/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Experience_ProfilDetailComponent } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil-detail.component';
import { Experience_ProfilService } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.service';
import { Experience_Profil } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.model';

describe('Component Tests', () => {

    describe('Experience_Profil Management Detail Component', () => {
        let comp: Experience_ProfilDetailComponent;
        let fixture: ComponentFixture<Experience_ProfilDetailComponent>;
        let service: Experience_ProfilService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Experience_ProfilDetailComponent],
                providers: [
                    Experience_ProfilService
                ]
            })
            .overrideTemplate(Experience_ProfilDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Experience_ProfilDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Experience_ProfilService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Experience_Profil(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.experience_Profil).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
