/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Experience_ProfilComponent } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.component';
import { Experience_ProfilService } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.service';
import { Experience_Profil } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.model';

describe('Component Tests', () => {

    describe('Experience_Profil Management Component', () => {
        let comp: Experience_ProfilComponent;
        let fixture: ComponentFixture<Experience_ProfilComponent>;
        let service: Experience_ProfilService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Experience_ProfilComponent],
                providers: [
                    Experience_ProfilService
                ]
            })
            .overrideTemplate(Experience_ProfilComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Experience_ProfilComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Experience_ProfilService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Experience_Profil(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.experience_Profils[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
