/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Experience_ProfilDialogComponent } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil-dialog.component';
import { Experience_ProfilService } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.service';
import { Experience_Profil } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.model';
import { ProfilService } from '../../../../../../main/webapp/app/entities/profil';
import { ExperienceService } from '../../../../../../main/webapp/app/entities/experience';

describe('Component Tests', () => {

    describe('Experience_Profil Management Dialog Component', () => {
        let comp: Experience_ProfilDialogComponent;
        let fixture: ComponentFixture<Experience_ProfilDialogComponent>;
        let service: Experience_ProfilService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Experience_ProfilDialogComponent],
                providers: [
                    ProfilService,
                    ExperienceService,
                    Experience_ProfilService
                ]
            })
            .overrideTemplate(Experience_ProfilDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Experience_ProfilDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Experience_ProfilService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Experience_Profil(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.experience_Profil = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'experience_ProfilListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Experience_Profil();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.experience_Profil = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'experience_ProfilListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
