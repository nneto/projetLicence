/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Experience_ProfilDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil-delete-dialog.component';
import { Experience_ProfilService } from '../../../../../../main/webapp/app/entities/experience-profil/experience-profil.service';

describe('Component Tests', () => {

    describe('Experience_Profil Management Delete Component', () => {
        let comp: Experience_ProfilDeleteDialogComponent;
        let fixture: ComponentFixture<Experience_ProfilDeleteDialogComponent>;
        let service: Experience_ProfilService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Experience_ProfilDeleteDialogComponent],
                providers: [
                    Experience_ProfilService
                ]
            })
            .overrideTemplate(Experience_ProfilDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Experience_ProfilDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Experience_ProfilService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
