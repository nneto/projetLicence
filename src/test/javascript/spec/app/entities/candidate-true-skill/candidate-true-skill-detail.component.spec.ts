/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Candidate_true_skillDetailComponent } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill-detail.component';
import { Candidate_true_skillService } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.service';
import { Candidate_true_skill } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.model';

describe('Component Tests', () => {

    describe('Candidate_true_skill Management Detail Component', () => {
        let comp: Candidate_true_skillDetailComponent;
        let fixture: ComponentFixture<Candidate_true_skillDetailComponent>;
        let service: Candidate_true_skillService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Candidate_true_skillDetailComponent],
                providers: [
                    Candidate_true_skillService
                ]
            })
            .overrideTemplate(Candidate_true_skillDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Candidate_true_skillDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Candidate_true_skillService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Candidate_true_skill(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.candidate_true_skill).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
