/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Candidate_true_skillDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill-delete-dialog.component';
import { Candidate_true_skillService } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.service';

describe('Component Tests', () => {

    describe('Candidate_true_skill Management Delete Component', () => {
        let comp: Candidate_true_skillDeleteDialogComponent;
        let fixture: ComponentFixture<Candidate_true_skillDeleteDialogComponent>;
        let service: Candidate_true_skillService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Candidate_true_skillDeleteDialogComponent],
                providers: [
                    Candidate_true_skillService
                ]
            })
            .overrideTemplate(Candidate_true_skillDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Candidate_true_skillDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Candidate_true_skillService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
