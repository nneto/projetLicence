/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Candidate_true_skillDialogComponent } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill-dialog.component';
import { Candidate_true_skillService } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.service';
import { Candidate_true_skill } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.model';
import { ProfilService } from '../../../../../../main/webapp/app/entities/profil';
import { SkillService } from '../../../../../../main/webapp/app/entities/skill';

describe('Component Tests', () => {

    describe('Candidate_true_skill Management Dialog Component', () => {
        let comp: Candidate_true_skillDialogComponent;
        let fixture: ComponentFixture<Candidate_true_skillDialogComponent>;
        let service: Candidate_true_skillService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Candidate_true_skillDialogComponent],
                providers: [
                    ProfilService,
                    SkillService,
                    Candidate_true_skillService
                ]
            })
            .overrideTemplate(Candidate_true_skillDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Candidate_true_skillDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Candidate_true_skillService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Candidate_true_skill(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.candidate_true_skill = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'candidate_true_skillListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Candidate_true_skill();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.candidate_true_skill = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'candidate_true_skillListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
