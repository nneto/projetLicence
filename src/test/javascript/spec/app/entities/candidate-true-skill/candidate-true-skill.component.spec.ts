/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ProjetLicenceTestModule } from '../../../test.module';
import { Candidate_true_skillComponent } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.component';
import { Candidate_true_skillService } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.service';
import { Candidate_true_skill } from '../../../../../../main/webapp/app/entities/candidate-true-skill/candidate-true-skill.model';

describe('Component Tests', () => {

    describe('Candidate_true_skill Management Component', () => {
        let comp: Candidate_true_skillComponent;
        let fixture: ComponentFixture<Candidate_true_skillComponent>;
        let service: Candidate_true_skillService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ProjetLicenceTestModule],
                declarations: [Candidate_true_skillComponent],
                providers: [
                    Candidate_true_skillService
                ]
            })
            .overrideTemplate(Candidate_true_skillComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(Candidate_true_skillComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Candidate_true_skillService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Candidate_true_skill(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.candidate_true_skills[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
